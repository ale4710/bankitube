var continueWatching = {
    ready: false,
    id: ''
};

sessionStorage.removeItem('pagehistory');

window.addEventListener('DOMContentLoaded',()=>{
    requestContinueVideo();
});

function requestContinueVideo() {
    var testSrcs = getSettingValue('homescreen-top-display-mode'),
    availSrcs = [],
    srcsToCheck = [0, testSrcs.length],
    chosenSrc = -1,
    rv = '',
    ph = window.parent.allPageHistory,

    addAvailSrc = (id, exdata)=>{
        availSrcs.push({
            id: id,
            data: exdata || null
        });
    },

    requestContinueVideoFinish = ()=>{
        focusContWatching(false);
        continueWatching.ready = false;
        continueWatching.id = rv;
    
        var cachedInfo = itemCache('video', continueWatching.id, 'get');
    
        if(cachedInfo) {
            console.log('topsection: got data from cache instead.');
            continueVideoDone(cachedInfo);
        } else {
            requestItem(
                'videos',
                rv,
                '',
                continueVideoDone,
                continueVideoError,
                false,
                true
            );
            document.getElementById('recent-video-loading-cover').classList.remove('hidden');
        }
    },
    
    
    srcCheckDone = ()=>{
        srcsToCheck[0]++;

        if(srcsToCheck[0] === srcsToCheck[1]) {
            if(availSrcs.length === 0) {
                continueVideoNoneAvail();
                return;
            } else {
                chosenSrc = availSrcs[Math.floor(Math.random() * availSrcs.length)];
            }
        
            switch(chosenSrc.id) {
                case 0: 
                    rv = localStorage.getItem('recentVideo');
                    eid('recent-video-header').textContent = i18n('homescreen-tops-continue');
                    requestContinueVideoFinish();
                    break;
                case 1:
                    var vidPool = [];
                    ph.iterate((v,k)=>{
                        if(k !== 'lastItem') {
                            if(v.type === 'video' && vidPool.indexOf(v.id) === -1) {
                                vidPool.push(v.id);
                            }
                        }
                    }).then(()=>{
                        rv = selectRandomFromArray(vidPool);
                        requestContinueVideoFinish();
                    });
                    eid('recent-video-header').textContent = i18n('homescreen-tops-history');
                    break;
                case 2:
                    rv = selectRandomFromArray(randomVideoList());
                    eid('recent-video-header').textContent = i18n('homescreen-tops-random');
                    requestContinueVideoFinish();
                    break;
            }
        }
    };

    if(srcsToCheck[1] === 0) {
        continueVideoNoneAvail();
    } else {
        //check if this src is possible
        for(var i = 0; i < testSrcs.length; i++) {
            switch(testSrcs[i]) {
                case 0: 
                    if(localStorage.getItem('recentVideo') !== null) {
                        addAvailSrc(testSrcs[i]);
                    }
                    srcCheckDone();
                    break;
                case 1:
                    (()=>{
                        var li = i; //saving i in a local function scope.
                        //async is pretty cool but really irritating at times.
                        ph.length().then((l)=>{
                            if(l !== 0) {
                                addAvailSrc(testSrcs[li]);
                            }
                            srcCheckDone();
                        });
                    })();
                    
                    break;
                case 2:
                    //case 2 will never need to be checked, because it is always valid.
                    addAvailSrc(testSrcs[i]);
                    srcCheckDone();
                    break;
            }
        }
    }

    
}
function continueVideoError() {
    document.getElementById('recent-video-title').textContent = `${i18n('error')} [ID: ${continueWatching.id}]`;
    document.getElementById('recent-video-thumbnail').src = '/img/nothumbnail.png';
    continueVideoFinalize();
}

function continueVideoNoneAvail() {
    eid('home-menu').classList.add('vertical-center');
    eid('recent-video-cont').classList.add('hidden');
}

function continueVideoDone(rqResp) {
    itemCache('video', continueWatching.id, 'update', rqResp);
    var vthumb = thumbchoose(rqResp.videoThumbnails);
    document.getElementById('recent-video-title').textContent = rqResp.title;
    document.getElementById('recent-video-author').textContent = rqResp.author;
    document.getElementById('recent-video-viewcount').textContent = commaSeparateNumber(rqResp.viewCount);
    document.getElementById('recent-video-published').textContent = unixdateformat(rqResp.published);
    document.getElementById('recent-video-runtime').textContent = timeformat(rqResp.lengthSeconds);
    document.getElementById('recent-video-thumbnail').src = vthumb;

    subscriptionsUpdateMdata(
        rqResp.authorId,
        rqResp.author,
        imgChoose(rqResp.authorThumbnails,'width',100)
    );

    mdataStoreSet(
        continueWatching.id,
        {
            name: rqResp.title,
            type: 'video',
            author: {
                name: rqResp.author,
                id: rqResp.authorId
            },
            time: rqResp.published,
            length: rqResp.lengthSeconds,
            image: vthumb
        }
    );

    continueVideoFinalize();
}

function continueVideoFinalize() {
    continueWatching.ready = true;
    document.getElementById('recent-video-loading-cover').classList.add('hidden');
}