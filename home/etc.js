if(getSettingValue('show-version-on-homemenu') === 1) {
	var v = document.createElement('span');
	v.classList.add('version-label');
	v.textContent = `(${versionNumber})`;
	eid('main-menu-header').appendChild(v);
	v = undefined;
}
