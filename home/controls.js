disableControls = false;
scrollMenu(1);
updatenavbar();
updateLabel();
allowBack = false;

function localupdatenavbar() {
    var i = actEl().id === 'recent-video'? [
        //thing is recent video
        i18n('watch'),
        i18n('listen')
    ] : [
        //thing is not recent video
        i18n('select'),
        ''
    ];
    outputNavbar(
        i18n('menu'),
        i[0],
        i[1]
    );
    updateLabel();
}
function keyHandler(k) {
    switch(k.key) {
        case 'ArrowLeft': scrollMenu(-1); break;
        case 'ArrowRight': scrollMenu(1); break;
        case 'ArrowUp': focusContWatching(true); break;
        case 'ArrowDown': focusContWatching(false); break;
        case 'Enter': enterMenu(); break;
        case 'SoftRight': enterMenu('true'); break;
        case 'SoftLeft':  navMainMenu(0); break;
        case 'Backspace': 
            messageBox(
                i18n('are-you-sure'),
                i18n('exit-confirmation'),
                {
                    left: messageBoxOption(
                        messageBoxDefaultBackCallback,
                        i18n('no')
                    ),
                    right: messageBoxOption(
                        ()=>{
                            window.parent.close();
                        },
                        i18n('yes')
                    )
                }
            );
            break;
    }
}

function focusContWatching(tr) {
    if(tr) {
        if(continueWatching.ready) {
            document.getElementById('recent-video').focus();
        }
    } else {
        if(actEl().id === 'recent-video'){
            document.getElementById('home-menu-btns').children[0].focus();
        }
    }
}

function scrollMenu(dir) {
    if(actEl().id !== 'recent-video') {
        navigatelist(
            actEl().tabIndex,
            document.getElementById('home-menu-btns').children,
            dir
        );

    }
}
function updateLabel() {
    document.getElementById('home-menu-label').textContent = [
        '',
        i18n('search'),
        i18n('feed'),
        i18n('playlists-menu-item'),
        i18n('history'),

    ][actEl().tabIndex + 1];
}
function enterMenu(listen) {
    if(actEl().id === 'recent-video') {
        if(listen) {listen = '&listen=true'} else {listen = ''}
        location = '/watch/index.html#v=' + continueWatching.id + listen;
    } else {
        if(listen) {return}
        switch(actEl().tabIndex) {
            case 0: location = '/search/index.html'; break;
            case 1: goToSearchScreen(null, 'subscriptions'); break;
            case 2: goToSearchScreen(null, 'userPlaylistList'); break;
            case 3: location = '/history/index.html'; break;
        }
    }
}