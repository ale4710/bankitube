var mdataStore = localforage.createInstance({
    name: 'BankiTube',
    storeName: 'mdataStore',
});

    /* 
    standard names
    (watch me break these rules lol)

    name - string - the name of whatever object it is. does not changes whether its a playlist, video, user, etc.
    author - object - the creator of whatever object it is. if it is a user this shouldnt be set.
        name - string - name
        id - string - id
    time - number - when the object was created.
    image - string - the image. thumbnail or profile picture. 
        video fallback url: https://i3.ytimg.com/vi/[video_id]/default.jpg
    type - string - type.

    any of these can be omitted if needed.

    reserved names
    id
    */


function mdataStoreSet(id, data, doneFn, errFn) {
    mdataStore.getItem(id).then((r)=>{
        if(r) {
            objectMerge(
                r,
                data,
                true
            );
        }

        mdataStore.setItem(id, data)
            .then(doneFn || emptyFn)
            .catch(errFn || emptyFn);
    }).catch(errFn);
}

function mdataStoreRmnr() {
    console.log('mdataStore: cleaning: job started.');

    var md = [], cinmd = (id)=>{
        var inmd = md.indexOf(id);
        //console.log(id, inmd);
        if(inmd !== -1) {
            md.splice(inmd,1);
        }
    };

    //we can do the following all at once instead of one after another to make it faster.
    //ill do it later.

    mdataStore.keys().then((kagi)=>{ //got keys
        console.log(kagi);
        md = kagi;
        kagi = undefined; //just to save memory.
        console.log('mdataStore: cleaning: got all keys. checking for references now.');
        channelSubscriptions.keys().then((subKagi)=>{
            subKagi.forEach(cinmd);
            console.log('mdataStore: cleaning: done iterating subscriptions. iterating history now.');
            allPageHistory.iterate((v)=>{
                cinmd(v.id);
            }).then(()=>{ //iterate done. now iterating playlists.
                console.log('mdataStore: cleaning: done iterating history. iterating playlists now.');
                userPlaylistStore.iterate((plst)=>{ //get playlist
                    plst.forEach(cinmd); //check all playlist items
                }).then(()=>{ //iterate done.
                    console.log(`mdataStore: cleaning: there are ${md.length} entries with no references. now deleting.`);
                    var delc = 0, del = ()=>{
                        if(delc in md) {
                            mdataStore.removeItem(md[delc]).then(del);
                            delc++;
                        } else {
                            console.log('mdataStore: cleaning: ok, all done~!');
                        }
                    }
                    del();
                });
                
            });
        });
    });
}
