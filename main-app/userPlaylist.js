var userPlaylistStore = localforage.createInstance({
    name: 'BankiTube',
    storeName: 'userPlaylists'
}),

userPlaylistMdataStore = localforage.createInstance({
    name: 'BankiTube',
    storeName: 'userPlaylistMdata'
}), //to see what the objects in here look like see userPlaylistManage -> switch(action) case 0.

userPlaylistMdataCache = {},

playlistErrors = {};

(()=>{ //populate playlistErrors
    [
        'canceled',
        'notExist',
        'alreadyExists',
        'invalidData'
    ].forEach((n,i)=>{
        playlistErrors[n] = {
            id: i, name: n
        };
    });
})();

refreshUserPlaylistCache();
function refreshUserPlaylistCache() {
    userPlaylistMdataCache = {};

    userPlaylistMdataStore.iterate((val,key)=>{
        if(key.charAt(0) === 'u') {
            key = key.substr(1);
            userPlaylistMdataCache[key] = val;
        }
    });
}

function clearPlaylistData(cb) {
    var f = (e)=>{console.error(e);};
    userPlaylistStore.clear().then(()=>{
        userPlaylistMdataStore.clear().then(()=>{
            if(typeof(cb) === 'function') {
                cb();
            }
            console.log('playlist data cleared.');
        }).catch(f);
    }).catch(f);
}

//returns {error: 'nameTaken'} to errFn if the name already exists.
/* actions:
0 - create
    plid will instead be the new name of the playlist.
    the id will be returned to doneFn.
1 - delete
2 - rename
    exdata will be the new name of the playlist.
*/
function userPlaylistManage(plid, action, exdata, doneFn, errFn) {
    var mdplid = 'u' + plid,
    
    cdi = 0, cd = (rv)=>{
        if(++cdi === 2) {
            doneFn(rv);
        }
    };

    switch(action) {
        case 0: //create
            if(typeof(plid) === 'string') {
                userPlaylistStore.keys().then((keys)=>{
                    var cdwrv = ()=>{cd(keyToUse)},
                    keyToUse = '';

                    while(
                        keys.indexOf(keyToUse) > -1 ||
                        keyToUse === ''
                    ) {
                        keyToUse = randomHexString();
                    }
                    console.log('playlist id will be', keyToUse);

                    userPlaylistStore.setItem(keyToUse, []).then(cdwrv).catch(errFn);

                    var mdsBlankEntry = {
                        name: plid,
                        count: 0,
                        image: null
                    };
                    userPlaylistMdataStore.setItem('u' + keyToUse, mdsBlankEntry).then(cdwrv).catch(errFn);

                    userPlaylistMdataCache[keyToUse] = mdsBlankEntry;
                }).catch(errFn);
            } else {
                console.error('userPlaylistManage: name was not a string.');
                errFn({error: playlistErrors.invalidData});
            }
            break;
        //the following requre checking to see if the playlist id exists.
        case 1: //del
        case 2: //rename
            userPlaylistMdataStore.getItem(mdplid).then((r)=>{
                switch(action) {
                    case 1: //delete
                        if(r !== null) {
                            userPlaylistStore.removeItem(plid).then(cd).catch(errFn);
                            userPlaylistMdataStore.removeItem(mdplid).then(cd).catch(errFn);
                            delete userPlaylistMdataCache[plid];
                        } else {
                            console.error('userPlaylistModify: the playlist does not exist.');
                            errFn({error: playlistErrors.notExist});
                        }
                        break;
                    case 2: //rename
                        if(r !== null) {
                            if(typeof(exdata) === 'string') {
                                r.name = exdata;
                                //only modifying metadata, no need to do cd()
                                userPlaylistMdataStore.setItem(mdplid, r).then(doneFn).catch(errFn);
                                userPlaylistMdataCache[plid].name = exdata;
                                break;
                            } else {
                                console.error('userPlaylistModify: the new name was not a string.');
                                errFn({error: playlistErrors.invalidData});
                            }
                        } else {
                            console.error('userPlaylistModify: the playlist does not exist.');
                            errFn({error: playlistErrors.notExist});
                        }
                        break;
                }
            });
            break;
    }
    
}

/* 
0 - add video
    video id, null
1 - remove video
    index of video to remove, null
2 - move video
    index of video to move, target index
*/
function userPlaylistModify(playlist,action,param1,param2,doneFn,errFn) {
    userPlaylistStore.getItem(playlist).then((r)=>{
        if(r !== null) {
            var mdataChanges = [],
            rinitlen = r.length;
            switch(action) {
                case 0: //add video
                    if(r.indexOf(param1) === -1) {
                        r.push(param1);
                    } else {
                        console.error('userPlaylistModify: video', param1, 'already exists.');
                        errFn({error: playlistErrors.alreadyExists});
                        return;
                    }
                    break;
                case 1: //remove video
                    if(param1 < r.length) {
                        r.splice(param1, 1);
                    } else {
                        console.error('userPlaylistModify: invalid remove index. tried to remove video at', param1, 'but max length is', r.length);
                        errFn({error: playlistErrors.notExist});
                        return;
                    }
                    break;
                case 2: //move video
                    if(
                        param1 < r.length &&
                        param2 < r.length
                    ) {
                        arrayMove(r, param1, param2);
                    } else {
                        console.error('userPlaylistModify: invalid move from or move to number. from/to:', param1, param2);
                        errFn({error: playlistErrors.notExist});
                        return;
                    }
                    break;
            }

            if(rinitlen !== r.length) {
                mdataChanges.push(
                    makeUPMMDaction(
                        UPMMDactions.modifyCount,
                        r.length,
                        true
                    )
                );
            }

            //playlist is always modified, so we will just push it anyway
            mdataChanges.push(
                makeUPMMDaction(
                    UPMMDactions.updateThumbnail,
                    r[0]
                )
            );

            var prog = 0, checkDone = ()=>{
                if(++prog === 2) {
                    doneFn();
                }
            };

            if(mdataChanges.length !== 0) {
                userPlaylistModifyMdata(
                    playlist,
                    mdataChanges,
                    checkDone,
                    errFn
                );
            }

            userPlaylistStore.setItem(playlist,r).then(checkDone).catch(errFn);
        } else {
            console.error('userPlaylistModify: the playlist does not exist.');
            errFn({error: playlistErrors.notExist});
        }
    }).catch(errFn);
}

var UPMMDactions = {
    modifyCount: 0,
    updateThumbnail: 1
};
function makeUPMMDaction(action) {
    action = {
        action: action
    };

    switch(action.action) {
        case UPMMDactions.modifyCount:
            action.count = arguments[1];
            action.absolute = arguments[2];
            break;

        case UPMMDactions.updateThumbnail:
            action.thumbnail = arguments[1];
            break;
    }

    return action;
}
function userPlaylistModifyMdata(pid, actions, doneFn, errFn) {
    var upid = 'u' + pid;
    userPlaylistMdataStore.getItem(upid).then((r)=>{

        actions.forEach((ta)=>{
            switch(ta.action) {
                case UPMMDactions.modifyCount:
                    if(!ta.absolute) {ta.count += r.count;}
                    r.count = ta.count;
                    break;
        
                case UPMMDactions.updateThumbnail:
                    r.image = ta.thumbnail;
                    break;
            }
        });

        userPlaylistMdataStore.setItem(upid, r).then(()=>{
            userPlaylistMdataCache[pid] = r;
            doneFn();
        }).catch(errFn);
    }).catch(errFn);
}