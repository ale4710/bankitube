var allPageHistory = localforage.createInstance({
    name: 'BankiTube',
    storeName: 'history',
}),
internalPageTypes = [
    'dummy',
    'historypage',
    'subscriptionspage',
    'subscriptionsfeed',
    'settingspage'
],
excludeMdataStore = [
    'search'
],
historyMaxLength = 1000;

function pushToAllHistory(id,type,name,exdata,timeOverride,successFunc) {
    if(getSettingValue('record-history') === 0) {return}
    if(([
        'channelvideos',
        'channelplaylists'
    ].concat(internalPageTypes).indexOf(type)) !== -1
    ) {
        return;
    }
    
    exdata = exdata || {};

    if(name && excludeMdataStore.indexOf(type) === -1) {
        mdataStoreSet(
            id,
            {
                name: name,
                type: type
            }
        )
    }


    allPageHistory.length().then((l)=>{
        var lcf = (an)=>{
            var hi = {
                id: id,
                type: type,
                exdata: exdata || {},
                time: timeOverride || (new Date()).getTime() / 1000
            };

            if(excludeMdataStore.indexOf(type) !== -1) {
                hi.name = name;
            }

            allPageHistory.setItem(an, hi).then(
                successFunc || emptyFn
            );
        };
        if(l >= historyMaxLength) {
            allPageHistory.getItem('lastItem').then((r)=>{
                if(
                    typeof(r) === 'number' &&
                    r !== historyMaxLength
                ) {
                    r += 1;
                } else {
                    r = 0;
                }
                allPageHistory.setItem('lastItem', r);
                lcf(r);
            });
        } else {
            lcf(l);
        }
    }).catch(console.error);
}