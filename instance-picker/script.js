








/* this code is disgusting, please don't look at it. */









window.addEventListener('DOMContentLoaded',()=>{
    disableControls = false;
    allowBack = false;
    viewLocalList();
    ecls('instance')[0].focus();
    updatenavbar();
    pushHistory('dummy');
});

var onlineList = [], 
viewingOL = false,
probing = {
    rqs: [],
    done: 0
};

function getOnlineList() {
    freedatagrab(
        'https://api.invidious.io/instances.json?sort_by=type,users',
        onlineListLoadSuccess,
        ()=>{
            eid('loader').classList.add('hidden');
            disableControls = false;
        },
        true,
        true
    );
    eid('loader').classList.remove('hidden');
    disableControls = true;
}
function onlineListLoadSuccess(list) {
    disableControls = false;
    eid('instances').innerHTML = '';
    list.forEach((e)=> {
        if(e[1].type === 'https') {

            //programming is insanely annoying!
            var usage;

            if(e[1].stats) {
                if('usage' in e[1].stats) {
                    usage = e[1].stats.usage.users.activeMonth
                }
            }

            if(!usage) {
                usage = '?';
            }

            var ti = {
                name: e[0],
                region: e[1].region || '?',
                users: usage,
                url: e[1].uri
            };

            addInstanceToList(
                ti.name,
                ti.region,
                ti.users,
                ti.url
            );

            onlineList.push(ti);
        }
    });
    eid('loader').classList.add('hidden');
    viewingOL = true;
    eid('online-switcher').textContent = i18n('instance-picker-switch-to-local');
    ecls('instance')[0].focus();
    console.log('viewing online list');
}

function toggleOnlineList() {
    if(probing.done !== probing.rqs.length) {
        testAllAbort();
    }

    if(viewingOL) {
        viewLocalList();
        viewingOL = false;
        eid('online-switcher').textContent = i18n('instance-picker-switch-to-online');
        console.log('viewing local list');
    } else {
        if(onlineList.length === 0) {
            getOnlineList();
        } else {
            viewingOL = true;
            console.log('viewing online list');
            eid('online-switcher').textContent = i18n('instance-picker-switch-to-local');
            eid('instances').innerHTML = '';
            onlineList.forEach((e)=>{
                addInstanceToList(
                    e.name,
                    e.region,
                    e.users,
                    e.url
                );
            });
            ecls('instance')[0].focus();
        }
    }
}

function viewLocalList() {
    eid('instances').innerHTML = '';
    [
        'https://youtube.076.ne.jp',
        'https://invidious.40two.app',
        'https://invidious.snopyta.org',
        'https://ytprivate.com',
        'https://invidious.tube',
        'https://invidious.fdn.fr',
        'https://invidious.namazso.eu',
        'https://invidious.himiko.cloud',
        'https://vid.puffyan.us',
        'https://inv.skyn3t.in',
        'https://invidious.ethibox.fr',
        'https://invidious.zee.li',
        'https://invidious.zapashcanon.fr',
        'https://vid.mint.lgbt'
    ].forEach((e)=>{
        addInstanceToList(e.substr('https://'.length), '?', '?', e);
    });

    ecls('instance')[0].focus();
}

function addInstanceToList(name, region, users, uri) {
    var itemrow = document.createElement('tr'),
    items = [
        document.createElement('td'),
        document.createElement('td'),
        document.createElement('td')
    ];
    
    items[0].textContent = name;
    items[0].classList.add('instance-url');
    items[0].style.width = '75%';

    items[1].textContent = region;
    items[1].classList.add('instance-region');
    items[1].style.width = '10%';

    items[2].textContent = users;
    items[2].classList.add('instance-users');
    items[2].style.width = '15%';


    items.forEach((r)=>{itemrow.appendChild(r)});
    
    itemrow.tabIndex = eid('instances').children.length;
    itemrow.classList.add('instance', 'focusable-item');
    itemrow.dataset.url = uri;
    eid('instances').appendChild(itemrow);

}

function testAll() {
    var instances = ecls('instance'),
    testVideo = randomVideoList();

    eid('test-instances').textContent = i18n('instance-picker-probe-instances-abort');

    testVideo = testVideo[Math.floor(testVideo.length * Math.random())];

    probing.done = 0;
    probing.rqs = [];

    for(var i = 0; i < instances.length; i++) {
        instances[i].classList.add('waiting');
        instances[i].classList.remove('success', 'fail', 'nonexistant', 'blocked', 'warning', 'inconclusive');
        probing.rqs.push(
            freedatagrab(
                instances[i].dataset.url + rqApi.suffix + '/videos/' + testVideo,
                testAllOneDone,
                testAllOneDone,
                false,
                false,
                i
            )
        );
    }
}

function testAllOneDone(r, i, ro) {
    var instances = ecls('instance');
    instances[i].classList.remove('waiting');
    probing.done++;
    if(probing.rqs.length === probing.done) {eid('test-instances').textContent = i18n('instance-picker-probe-instances');}
    switch((ro || r).status) {
        case 0:
            instances[i].classList.add('nonexistant');
            return;
        case 502:
        case 503:
            instances[i].classList.add('fail');
            return;
        case 403: 
            instances[i].classList.add('blocked'); 
            break;
    }
    var d = pJSON(r);

    if(d === undefined) {
        instances[i].classList.add('fail');
        return;
    }

    var estatus = 
    ('formatStreams' in d) +
    (('error' in d) << 1)
    ;

    switch(estatus) {
        case 1: //format streams exist, no error
            instances[i].classList.add('success');
            break;
        case 0: //nothing??
        case 2: //format streams dont exist, with error
            instances[i].classList.add('fail');
            break;
        case 3: //format streams exist, with error
            if(d.error === 'Could not extract video info. Instance is likely blocked. ') {
                instances[i].classList.add('warning');
            } else {
            instances[i].classList.add('success');
            }
            break;
    }

}

function testAllAbort() {
    for(var i = 0; i < probing.rqs.length; i++) {
        probing.rqs[i].abort();
    }
}