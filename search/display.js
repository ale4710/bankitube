/* output */
var searchResultsElement = null,
output = {
    name: eid('current-info-name'),
    video: {
        self: eid('current-info-video'),
        views: eid('current-info-video-views'),
        published: eid('current-info-video-date'),
        author: eid('current-info-video-author'),
        description: eid('current-info-video-description')
    },
    playlistDetails: {
        self: eid('current-info-playlist-details'),
        videocount: eid('current-info-playlist-details-count'),
        published: eid('current-info-playlist-details-date'),
        author: eid('current-info-playlist-details-author'),
        description: eid('current-info-playlist-details-description')
    },
    playlistVideo: {
        self: eid('current-info-video-in-playlist'),
        author: eid('current-info-video-in-playlist-author'),
        index: eid('current-info-video-in-playlist-index')
    },
    playlist: {
        self: eid('current-info-playlist'),
        author: eid('current-info-playlist-author'),
        videocount: eid('current-info-playlist-videocount')
    },
    channel: {
        self: eid('current-info-channel'),
        subcount: eid('current-info-channel-subcount'),
        videocount: eid('current-info-channel-videocount'),
        description: eid('current-info-channel-description')
    }
};

function updateInfo(blank) {
    if(blank) {
        infoPanelShow(-1);
        output.name.textContent = '';
    } else {
        var c = actEl().tabIndex, sr = searchResults;
        if(sr[c].type === 'channel') {
            output.name.textContent = sr[c].author.name;
        } else {
            output.name.textContent = sr[c].name;
        }
    
        switch(sr[c].type) {
            case 'video': 
            case 'videoFromSubscription':
                infoPanelShow(0);
                var o = output.video;
                o.views.textContent = sr[c].count;
                o.published.textContent = unixdateformat(sr[c].updated);
                o.author.textContent = sr[c].author.name;
                o.description.textContent = sr[c].description;
                break;
            case 'playlistVideo': infoPanelShow(3);
                var o = output.playlistVideo;    
                o.author.textContent = sr[c].author.name;
                o.index.textContent = sr[c].count + 1;
                break;
            case 'playlistDetails':infoPanelShow(4);
                var o = output.playlistDetails;
                o.videocount.textContent = i18n(
                    'video-count',
                    sr[c].count, 
                    {count: commaSeparateNumber(sr[c].count)}
                );
                o.published.textContent = sr[c].updated;
                o.author.textContent = sr[c].author.name;
                o.description.textContent = sr[c].description.plain;
                break;
            case 'playlist':
            case 'userPlaylist':
                infoPanelShow(1);
                var o = output.playlist;
                o.author.textContent = sr[c].author.name;
                o.videocount.textContent = i18n(
                    'video-count',
                    sr[c].count, 
                    {count: commaSeparateNumber(sr[c].count)}
                );
                break;
            case 'channel':infoPanelShow(2);
                var o = output.channel;
                o.subcount.textContent = i18n(
                    'subscriber-shortened-count',
                    sr[c].author.subcount,
                    {count: commaSeparateNumber(sr[c].author.subcount)}
                );
                o.videocount.textContent = i18n(
                    'video-count',
                    sr[c].count, 
                    {count: commaSeparateNumber(sr[c].count)}
                );
                o.description.textContent = sr[c].description;
                break;
            case 'more':
            case 'dead':
                infoPanelShow(-1);break;
    
        }
    
        nameScroll.reset();
    }
    
}

function infoPanelShow(type) {
    var all = ['video','playlist','channel','playlistVideo','playlistDetails'],
    ha = () => {all.forEach((e) => {output[e].self.classList.add('hidden');});}
    if(type in all) {
        type = all[type];
    } else if(type === -1) {
        ha();
        return;
    } else {
        console.error('infoPanelShow: no such panel to show');
        return;
    }
    ha();
    output[type].self.classList.remove('hidden');
}

var nameScroll = new ScrollingText(
    output.name,
    null,
    5
);

var displayModeNames = [
    'carousel',
    'list'
];
function cdisplayModeName() {return displayModeNames[displayMode];}
function updateView() {
    //dm = displaymode, asds = all search displays
    var dm = cdisplayModeName(),
    asds = ecls('search-results-display');
    for(var i = 0; i < asds.length; i++) {
        asds[i].classList.add('hidden');
    }
    searchResultsElement = eid('search-results-display-' + dm);
    searchResultsElement.classList.remove('hidden');
    searchResultsElement = searchResultsElement.getElementsByClassName('search-results')[0];

    //specifc stuff
    switch(dm) {
        case 'carousel':
            nameScroll.play();
            break;
    }
}
function repostSearchResults() {
    searchResults.forEach((r,i)=>{
        outputElements[cdisplayModeName()].individual(
            r, //current result
            i, //results printed
            0 //offset
        )
    })
}

function showFreeTextScreen(text,asHtml) {
    var ft = eid('freetext-display');
    if(text === false) {
        ft.classList.add('hidden');
        curpage = 0;
        allowBack = true;
        focusOnActiveSearchEntry();
    } else {
        ft.classList.remove('hidden');
        curpage = 2;
        allowBack = false;
        ft.scrollTop = 0;
        ft.focus();
        if(asHtml) {
            ft.innerHTML = text;
        } else {
            ft.innerText = text;
        }
    }
}
function freeTextK(k) {
    switch(k.key) {
        case 'Backspace': showFreeTextScreen(false); break;
        case 'SoftLeft': navMainMenu(0); break;
    }
}

/* div creation */
var displayMode = getSettingValue('default-list-view'),
outputElements = {
    carousel: {
        individual: (cres, resPr, start)=>{
            searchResultsElement.appendChild(
                outputElements.carousel.individualRE(cres, resPr, start)
            );
        },

        individualRE: (cres, resPr, start)=>{
            var el = document.createElement('div'),
            count = document.createElement('div'),
            img = document.createElement('img'),
            imgUrl;

            var imgAtp = returnProperty(
                cres,
                'images', 0
            );
            if(imgAtp.isIn) {
                imgUrl = cres.images[0]
            } else {
                imgUrl = '/img/nothumbnail.png';
            }

            el.tabIndex = start + resPr;

            count.classList.add("search-results-count");
            switch(cres.type) {
                case 'video': 
                case 'playlistVideo':
                    var txt;
                    if(cres.live) {
                        txt = i18n('live-broadcasting');
                        el.classList.add('search-results-live');
                    } else if(cres.upcoming) {
                        txt = i18n('premiere');
                    } else {
                        txt = timeformat(cres.length);
                    }
                    count.textContent = txt;
                    break;
                case 'playlist':
                case 'userPlaylist':
                    count.textContent = cres.count; 
                    el.classList.add('search-results-playlist');
                    break;
                case 'channel':
                    el.classList.add('search-results-channel');
                case 'more':
                case 'playlistDetails': 
                    count.classList.add("search-results-count-hidden"); 
                    break;
                case 'dead':
                    imgUrl = '/img/thumbnail-dead.png';
                    break;

            }

            //for lazyloading.
            if(
                resPr < 10 ||
                [
                    'more',
                    'playlistDetails'
                ].indexOf(cres.type) !== -1
            ) {
                img.src = imgUrl;
            } else {
                img.src = '/img/thumbnail-loading.png';
                img.dataset.src = imgUrl;
            }
            img.loading = 'lazy'; //NOT SUPPORTED IN KAIOS >:(

            el.appendChild(img);
            el.appendChild(count);

            return el;
        }
    },

    list: {
        individual: (cres, resPr, start)=>{
            var be = outputElements.carousel.individualRE(cres, resPr, start),
            el = document.createElement('div'),
            title = document.createElement('div'),
            subtitle = document.createElement('div'),
            ti = be.tabIndex;

            be.tabIndex = null;
            el.tabIndex = ti;
            //ti = undefined;
            

            var tcls = be.classList;
            for(var i = 0; i < tcls.length; i++) {
                el.classList.add(tcls[i]);
            }
            be.className = 'search-results-img';
            //tcls = undefined;

            defaultAction: {
                switch(cres.type) {
                    case 'channel': 
                        title.textContent = cres.author.name;
                        subtitle.textContent = i18n(
                            'subscriber-shortened-count',
                            cres.author.subcount,
                            {count: commaSeparateNumber(cres.author.subcount)}
                        );
                        break defaultAction;
                    case 'more':
                        title.textContent = cres.name;
                        //no subtitle
                        break defaultAction;
                }
                title.textContent = cres.name;
                subtitle.textContent = cres.author.name;
            }

            title.classList.add('search-results-title');
            subtitle.classList.add('search-results-subtitle');
            
            el.appendChild(be);
            el.appendChild(title);
            el.appendChild(subtitle);
            searchResultsElement.appendChild(el);

        }
    }
}