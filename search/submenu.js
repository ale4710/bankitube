var submenu = new OptionsMenu(),
submenuLastFocus = null,
submenuAllowed = [
    'video',
    'channel',
    'playlist',
    'playlistVideo',
    'playlistDetails',
    'videoFromSubscription',
    'userPlaylist'
];

function openSubMenu() {
    var csr = searchResults[actEl().tabIndex];
    if(submenuAllowed.indexOf(csr.type) === -1) {
        return false;
    } else {

        submenu.clearMenu();

        var headertext;
        if(csr.type === 'channel') {
            headertext = csr.author.name;
        } else {
            headertext = csr.name;
        }
        submenu.updateHeader(headertext);

        submenuLastFocus = actEl();
        var addItem = (label, action)=>{
            var addedItem = submenu.addOption(i18n(label));
            addedItem.dataset.action = action;
        };

        /* 
            1: listen
            2: save
            3: share
            4: move (playlist video)
            5: rename (playlist)
            6: remove
        */
        switch(csr.type) {
            case 'playlistVideo':
                addItem('listen', 1);
                addItem('save', 2);
                addItem('share-action', 3);
                if(type === 'userPlaylist') {
                    addItem('move', 4);
                    addItem('remove', 6);
                }
                break;

            case 'video':
            case 'videoFromSubscription':
                addItem('listen', 1);
                addItem('save', 2);
                addItem('share-action', 3);
                break;
                
            case 'channel':
                addItem('share-action', 3);
                break;

            case 'userPlaylist':
                addItem('rename', 5);
                addItem('playlist-delete', 6);
                break;

            case 'playlist':
            case 'playlistDetails':
                addItem('share-action', 3);
                if(returnProperty(
                    csr,
                    'extra','video'
                ).isIn) {addItem('listen-all', 1);}

                break;
        }

        curpage = 4;
        submenu.menuViewToggle(true, true);

        return true;
    }
}

function submenuDone() {
    curpage = 0;
    submenu.menuViewToggle(false);
    submenuLastFocus.focus();
    submenuLastFocus = null;
}

function submenuK(k) {
    switch(k.key) {
        case 'ArrowUp':
            var u = -1;
        case 'ArrowDown':
            submenu.navigate(u || 1);
            k.preventDefault();
            break;
        case 'Enter':
            var action = parseInt(actEl().dataset.action);
            submenuDone();
            itemAction(action);
            break;
        case 'Backspace':
        case 'SoftLeft':
            submenuDone();
            break;
    }
}