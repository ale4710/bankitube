var historyCategories = [
    '',
    'video',
    'channel',
    'playlist',
    'search'
], 
historyCategoryFocused = 0,
historyCategoriesTabs = null,
historyDoneRendering = false,
historyInitRendered = false,
historyIterateCancel = false;

window.addEventListener('DOMContentLoaded',()=>{
    historyCategoriesTabs = new Tabs(false, eid('history-tabs'));
    [
        'everything',
        'video-menu-item',
        'channel-menu-item',
        'playlists-menu-item',
        'searches'
    ].forEach((lb)=>{
        historyCategoriesTabs.addTab(i18n(lb));
    });

    window.parent.allPageHistory.length((l)=>{
        if(l !== 0) {
            focusCategory(0);
            disableControls = false;
            updatenavbar();
        } else {
            eid('no-entries-message').textContent = i18n('history-empty');
            eid('no-entries-message').classList.remove('hidden');
        }
    });
    pushHistory('historypage');

    window.addEventListener('historyEnumCancel', focusCurCategory);
});

function focusCategory(ch) {
    historyCategoryFocused = navigatelist(historyCategoryFocused,historyCategories,ch);
    historyCategoriesTabs.focusTab(historyCategoryFocused);

    if(
        !historyDoneRendering &&
        historyInitRendered
    ) {
        historyIterateCancel = true;
        console.log('waiting to cancel');
    } else {
        focusCurCategory();
    }
}

function focusCurCategory() {
    showHistory(historyCategories[historyCategoryFocused]);
    console.log('now showing current category');
}

function showHistory(category) {
    historyDoneRendering = false;
    historyIterateCancel = false;
    historyInitRendered = true;

    var bp = false, entries = [], totalEntries = 0, iterationDone = false, 
    entriesPush = (name, type, time, id)=>{
        entries.push({
            name: name,
            type: type,
            time: time,
            id: id
        });

        checkDone();
    },
    
    checkDone = ()=>{
        if(historyIterateCancel) {
            return;
        }

        if(
            iterationDone &&
            totalEntries === entries.length
        ) {
            eid('history').innerHTML = '';

            var entriesPosted = 0;
            entries.sort((a,b)=>{return a.time < b.time;});
            for(var i = 0; i < entries.length; i++) {
                var el = document.createElement('div'),
                ico = document.createElement('img'),
                name = document.createElement('div'),
                time = document.createElement('div'),
                
                ch = entries[i];
            
                ico.src = '/img/' + ch.type + '.png';
                el.appendChild(ico);
            
                name.textContent = ch.name;
                name.classList.add('history-item-name');
                el.appendChild(name);
            
                time.textContent = unixdatetimeformat(ch.time);
                el.appendChild(time);
            
                el.tabIndex = entriesPosted++;
                el.dataset.type = ch.type;
                el.dataset.id = ch.id;
                el.classList.add('history-item', 'focusable-item');
            
                eid('history').appendChild(el);
            }
            if(entriesPosted === 0) {
                eid('no-entries-message').textContent = i18n('history-none-in-category');
                eid('no-entries-message').classList.remove('hidden');
            }
            historyDoneRendering = true;
            navHistory(0);
            updatenavbar();
        }
    };


    if(!category) {bp = true}
    //hide stuff
    eid('no-entries-message').classList.add('hidden');
    eid('history').innerHTML = '';
    //show throbber
    var t = makeThrobber(true);
    t.classList.add('center');
    eid('history').appendChild(t);
    t = undefined;

    window.parent.allPageHistory.iterate((v)=>{
        if(historyIterateCancel) {
            console.log('checkDone: canceled.');
            return 1;
        }

        if(v.type === category || bp) {
            totalEntries++;

            if(window.parent.excludeMdataStore.indexOf(v.type) === -1) {
                mdataStore.getItem(v.id).then((res)=>{
                    rnc: {
                        if(res) { //res exists
                            if('name' in res) { //name in res
                                break rnc
                            }
                        }
                        
                        res = {};
                        res.name = makeUnknownTitleItem(v.id);
                    }
                    entriesPush(
                        res.name,
                        v.type,
                        v.time,
                        v.id
                    );
                });
            } else {
                entriesPush(
                    v.name,
                    v.type,
                    v.time,
                    v.id
                );
            }
        }
    }).then((cn)=>{
        if(cn) {
            historyDoneRendering = true;
            window.dispatchEvent(new Event('historyEnumCancel'));
            console.log('canceled!');
        } else {
            iterationDone = true;
            checkDone();
        }
    });
}

function navHistory(move) {
    if(historyDoneRendering) {
        var hc = eid('history').getElementsByClassName('history-item');
        if(0 in hc) {
            if(move === 0) {
                hc[0].focus();
            } else {
                navigatelist(actEl().tabIndex,hc,move);
            }
            scrolliv(
                actEl(),
                move>0,
                eid('history')
            );
        }
    }
    
}

function openHistoryEntry(e) {
    if(historyDoneRendering) {

        historyItemOpenSwitch(
            actEl().dataset.id,
            actEl().dataset.type,
            actEl().getElementsByClassName('history-item-name')[0].textContent,
            e
        );
    }
}