# localization!!!!!!!!
if you want to translate, look at [the documentation](http://i18njs.com/) for the translation library i used.

new strings are placed in a file located at `/common/lang/new-strings`. strings that you've translated should be placed at the top of the file, so that other translators can see what has been newly translated.

## layout

basically, it is this:

``` json
	{
		"settings": {
			"key": "value"
		},
		"translations": {
			"key": "translation"
		}
	}
```

it should be pretty obvious.

## settings

currently there is one setting.

### `capitalizeIsOkay`
boolean. default is `true`.

there are places where the text will be displayed with capital letters only. to prevent the program from doing this, set `capitalizeIsOkay` to `false`.

## about time and date formatting
the `date-format` and `time-format` key are definitions for time and date formatting.

example, if the day is 09 September 2009, this...

	(y)-(m)-(d)

...will produce this:

	2009-09-09

### definitions

#### date format

`(y)` full year  
`(m)` month  
`(d)` day

#### time format

`(_h)` 24hr non-zero-padded hour  
`(_hh)` 24hr zero-padded hour  
`(h)` 12hr non-zero-padded hour  
`(hh)` 12hr zero-padded hour  
`(m)` minutes  
`(s)` seconds  
`(af)` AM/PM