//see also: data.js

var addToPlaylist = {
    dialogue: new OptionsMenu(i18n('add-to-playlist')),
    showDialogue: (videoId, doneFn, lastPageOverride)=>{
        if(typeof(videoId) === 'string') {
            playlistActionVideoToAdd = videoId;
        } else {
            throw TypeError('VideoId was not a string.');
        }
        var palp = curpage;
        if(lastPageOverride !== undefined) {palp = lastPageOverride;}
        playlistActionLastPage = palp;
        playlistActionDoneAction = doneFn || null;
        curpage = 97;

        addToPlaylist.dialogue.clearMenu();
        addToPlaylist.dialogue.addOption(i18n('playlist-create-new'));

        var cplaylists = userPlaylistMdataCache();
        Object.keys(cplaylists).forEach((k)=>{
            var cpo = cplaylists[k];
            addToPlaylist.dialogue.addOption(cpo.name)
                .dataset.playlistId = k; //add to htmlelement which is returned above.
        });

        addToPlaylist.dialogue.menuViewToggle(true, true);
    }
},

createNewPlaylist = {
    dialogue: new OptionsMenuSelectable(i18n('playlist-create-new'), 'text'),
    showDialogue: (doneFn, lastPageOverride)=>{
        addToPlaylist.dialogue.menuViewToggle(false); //hide the menu just incase its showin
        if(lastPageOverride !== undefined) {palp = lastPageOverride;}
        if(typeof(doneFn) === 'function') {createNewPlaylist.doneFn = doneFn}
        curpage = 96;

        createNewPlaylist.dialogue.getValue().element.value = '';
        createNewPlaylist.dialogue.menuViewToggle(true, true);
    },
    doneFn: null
},

playlistActionLastPage = null,
playlistActionDoneAction = null,
playlistActionVideoToAdd = null;

function playlistUpdateNavbar() {
    outputNavbar(
        i18n('back'), 
        curpage === 97? i18n('select') : i18n('create'), 
        ''
    );
}

function playlistActionDone(result) {
    console.log('from userplaylist.js', result);
    addToPlaylist.dialogue.menuViewToggle(false);

    disableControls = false;

    if(
        !isNaN(playlistActionLastPage) && 
        typeof(playlistActionLastPage) === 'number'
    ) {
        curpage = playlistActionLastPage;
    }
    playlistActionLastPage = null;

    if(typeof(playlistActionDoneAction) === 'function') {
        playlistActionDoneAction(result);
        playlistActionDoneAction = null;
    }

    playlistActionVideoToAdd = null;
}

function userPlaylistK(k) {
    switch(k.key) {
        case 'ArrowUp': 
            var u = -1;
        case 'ArrowDown': 
            addToPlaylist.dialogue.navigate(u || 1); 
            break;
        case 'Enter': 
            var plid = actEl().dataset.playlistId,
            addVideoToPl = (tpl)=>{
                userPlaylistModify(
                    tpl,
                    0,
                    playlistActionVideoToAdd,
                    null,
                    /* (e)=>{
                        console.log('from userplaylistk success', e); playlistActionDone(e);
                    },
                    (e)=>{
                        console.log('from userplaylistk failure', e); playlistActionDone(e);
                    } */
                    playlistActionDone,
                    playlistActionDone
                );
                clearUserPlaylistCache(tpl);
                disableControls = true;
            };
            if(plid) {
                addVideoToPl(plid);
            } else {
                createNewPlaylist.showDialogue((nid)=>{
                    if(typeof(nid) === 'string') {
                        addVideoToPl(nid);
                    } else {
                        playlistActionDone(nid); 
                    }
                });
            }
            break;
        case 'SoftLeft':
        case 'Backspace': 
            playlistActionDone({error: getPlaylistErrors().canceled}); 
            break;
    }
}

function newUserPlaylistDoneAction(result) {
    createNewPlaylist.dialogue.menuViewToggle(false);

    if(typeof(createNewPlaylist.doneFn) === 'function') {
        createNewPlaylist.doneFn(result);
        createNewPlaylist.doneFn = null;
    } else {
        playlistActionDone(result);
    }
}
function newUserPlaylistK(k) {
    switch(k.key) {
        case 'Enter': 
            var pln = createNewPlaylist.dialogue.getValue().value;
            if(pln !== '') {
                userPlaylistManage(
                    pln,
                    0,
                    null,
                    newUserPlaylistDoneAction,
                    (e)=>{
                        alertMessage('this is a strange error.', 3000, 3);
                        console.error(e);
                        disableControls = false;
                    }
                );
                disableControls = true;
            }
            break;
        case 'SoftLeft':
        case 'Backspace': 
            newUserPlaylistDoneAction({error: getPlaylistErrors().canceled}); 
            break;
    }
}


//bs
function addToPlaylistSDAE(vid, callback, lpOverride) { //show dialouge auto error
    //this function will call what you put on callback regardless of error or not.
    callback = callback || emptyFn;
    addToPlaylist.showDialogue(
        vid,
        (result)=>{
            //console.log('from action.js', result);
            if(typeof(result) === 'object') {
                breakToEnd: {
                    //is error?
                    var err = returnProperty(
                        result,
                        'error', 'id'
                    );
                    if(err.isIn) {
                        switch(err.property) {
                            case 2: //alreadyexists
                                alertMessage(i18n('playlist-video-add-failure-already-exists'), 3000, 0);
                                break breakToEnd;
                            case 0: //canceled
                                break breakToEnd;
                        }
                    }

                    alertMessage(i18n('playlist-video-add-failure-generic'), 3000, 0);
                }
            } else {
                alertMessage(i18n('playlist-video-add-success'), 3000, 0);
            }
            
            callback();
        },
        lpOverride
    ); 
}

function getUserPlaylistASR(plid, callback) {
    callback = callback || emptyFn;

    userPlaylistStore.getItem(plid).then((playlistItems)=>{
        var resultsArray = [],
        resultsProcessed = [0, playlistItems.length];
        playlistItems.forEach((id, index)=>{
            mdataStore.getItem(id).then((mdata)=>{
                if(mdata === null) {
                    resultsArray[index] = {
                        videoId: id,
                        title: `${i18n('error')} [ID: ${videoId}]`,
                        author: null,
                        authorId: null,
                        lengthSeconds: 0,
                        videoThumbnails: []
                    };
                } else {
					let authorName = i18n('nanashi');
					let authorId = null;
					if(mdata.author) {
						authorName = mdata.author.name || authorName;
						authorId = mdata.author.id || authorId;
					}
					
                    resultsArray[index] = {
                        videoId: id,
                        title: mdata.name,
                        author: authorName,
                        authorId: authorId,
                        lengthSeconds: mdata.length || 0,
                        videoThumbnails: [{
                            quality: 'default',
                            url: mdata.image || linkUtil.formYtThumbnail(id)
                        }]
                    };
                }

                resultsArray[index].index = index;
                resultsArray[index].type = 'playlistVideo';

                resultsProcessed[0]++;
                if(resultsProcessed[0] === resultsProcessed[1]) {
                    callback(resultsArray);
                }
            });
        });
    });
}
function clearUserPlaylistCache(checkPlid) {
    if(
        sessionStorage.getItem('playlistCacheId') ===
        checkPlid
    ) {
        sessionStorage.removeItem('playlistCache');
        sessionStorage.removeItem('playlistCacheId');
        sessionStorage.removeItem('playlistCacheName');
    }
    
}