var
rqApi = {
    base: getSettingValue('invidious-instance'),
    suffix: '/api/v1'
};

function getUrlWithHostToApi(url) {
	url = new URL(url);
	url.hostname = (new URL(rqApi.base)).hostname;
	return url.toString();
}

function urlform(endpt) {return rqApi.base + rqApi.suffix + '/' + endpt;}


function requestItem(itemType, id, prms, success, error, notifyUser, parsejson, exdata) {
    /* var hl = 'hl=';
    if(prms) {prms += '&'} else {prms = ''}
    prms += hl; */
    return freedatagrab(
        urlform(itemType + '/' + id + '?' + prms),
        success, error, notifyUser, parsejson, exdata
    );
}

function freedatagrab(url, successFunc, errFunc, notifyUser, parseJSON, extraData) {
    if(!navigator.onLine) {
        alertMessage('No internet connection.',5000,3);
    }

    if(!errFunc) {errFunc = emptyFn;}
    if(!successFunc) {successFunc = emptyFn;}

    var rq = new XMLHttpRequest({
        mozSystem: true
    });

    rq.timeout = 60 * 1000 * 5; /* 5 minutes */

    rq.onerror = (e)=>{
        console.log(e.target.status);
        errFunc(e.target, extraData);
        if(notifyUser) {alertMessage('Failed to grab data.',5000,3);}
    }

    rq.onabort = (e)=>{
        errFunc(e.target, extraData);
    }

    rq.onload = (ev)=>{
        ev = ev.target;
        if(ev.readyState === 4) {
            console.log(ev.status);
            if(ev.status >= 400) {
                errFunc(ev, extraData);
                if(notifyUser) {alertMessage(`Server returned error code #${ev.status}.`,10000,3);}
                return;
            }
                try {
                    console.log(JSON.parse(ev.response));
                } catch(e) {
                    
                }
            
            if(parseJSON) {
                var evrParsed = ev.response;
                try {
                    evrParsed = JSON.parse(evrParsed);
                } catch(e) {
                    console.error('datagrabdone: could not parse rqResp as JSON');
                }
                successFunc(evrParsed, extraData, ev);
            } else {
                successFunc(ev.response, extraData, ev);
            }
        }
    };
    rq.open('GET',url);
    rq.send();

    return rq;
}
