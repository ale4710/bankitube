var thingToShare, sharelastpage, sharelastelement, shareActions = [
	{n: 'email', d: i18n('email')},
	{n: 'sms', d: i18n('sms')},
	{n:'whatsapp', d: 'Whatsapp'},
	{n:'twitter', d: 'Twitter'}
],
shareMenu = new OptionsMenu(i18n('share-via'));

function shareotInit() {
	shareActions.forEach((e) => {
		shareMenu.addOption(e.d);
	})
}

window.addEventListener('DOMContentLoaded',shareotInit);

function shareot(input,returnpage,returnel) {
	thingToShare = input;

	if(returnpage === null || returnpage === undefined) {returnpage = curpage}
	if(returnel === null || returnel === undefined) {returnel = actEl()}
	//doing the above instead of
	//	sharelastpage = curpage || returnpage
	//because 0 is false in javascript's logic
	sharelastpage = returnpage;
	sharelastelement = returnel;

	curpage = 100;
	shareMenu.menuViewToggle(true);
	shareMenu.menu.children[0].focus();
}

function shareCMA(method) { //share Call MozActivity
	method = shareActions[method].n;
	var mza;
	switch(method) {
		case 'email':
			mza = new MozActivity({
				name: 'new',
				data: {
					type: 'mail',
					URI: 'mailto:?body=' + encodeURIComponent(thingToShare), //for email
					//body: input //for text messaging
			}});
			break;
		case 'sms':
			mza = new MozActivity({
				name: 'new',
				data: {
					type: 'websms/sms',
					body: thingToShare //for text messaging
			}});
			break;
		case 'whatsapp':
			mza = new MozActivity({
				name: 'view',
				data: {
					type: 'url',
					url: 'https://wa.me/?text=' + encodeURIComponent(thingToShare)
			}});
			break;
		case 'twitter':
			mza = new MozActivity({
				name: 'view',
				data: {
					type: 'url',
					url: 'http://twitter.com/share?text=' + encodeURIComponent(thingToShare)
			}});
			break;
	}

	mza.onerror = function(e){
		console.log(e);
		switch(e.target.error.name) {
			case 'NO_PROVIDER': alertMessage(i18n('app-not-found'),5000,0); break;
			case 'ActivityCanceled': alertMessage(i18n('share-canceled'),5000,0); shareotReturn(); break;
			default: alertMessage(i18n('share-failed'),5000,0); break;
		}
	}
	mza.onsuccess = shareotReturn;
}

function shareotK(k) {
	switch(k.key) {
		case 'Backspace':
		case 'SoftLeft':
			shareotReturn();
			break;
		case 'ArrowUp':
			var u = -1;
		case 'ArrowDown': 
			navigatelist(
				actEl().tabIndex,
				shareMenu.menu.children,
				u || 1
			); 
			break;
		case 'Enter': shareCMA(Number(actEl().tabIndex));
	}
}
function shareotReturn() {
	shareMenu.menuViewToggle(false);
	curpage = sharelastpage;
	sharelastelement.focus();
}