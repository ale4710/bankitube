(()=>{
    var bg = '/common/';
    //scripts
    [

        //normal scripts
        'settings',
        'localization',
        'messageBox',
        'commonElements',
        'control',
        'etcf',
        'classes',
        'minimize',
        'scrolling',
        'mainmenu',
        'cache',
        'net',
        'share',
        'selecting',
        'data',
        'userPlaylist',
        'subscription',
        'randomVideoList',
    ].forEach((fn)=>{
        addGlobalReference(0, 
            bg + 'js/' + fn
        );
    });

    //styles
    [
        'all',
        'widgets',
        'theme/default'
    ].forEach((fn)=>{
        addGlobalReference(1, 
            bg + 'style/' + fn
        );
    });
})();