var mdataStore = window.parent.mdataStore,
mdataStoreSet = window.parent.mdataStoreSet;
//also see: /main-app/mdataStore.js

/* history */ ///////////////////////////////////////////////////////////////////
//also see: /main-app/allHistory.js

var pageHistory = sessionStorage.getItem('pagehistory'),
pushToAllHistory = window.parent.pushToAllHistory,
internalPageTypes = window.parent.internalPageTypes;


if(pageHistory) {
    pageHistory = JSON.parse(pageHistory);
} else {
    pageHistory = [];
}

function pushHistory(id,type,name,exdata) {
    if(internalPageTypes.indexOf(id) !== -1) {type = id; name = id;}
    var allow = true, hobj = {
        id: id,
        type: type,
        name: name,
        exdata: exdata || {}
    };

    //note: this only check the latest entry, so there aren't two entries right next to each other.
    latestCheck: if(pageHistory.length - 1 in pageHistory) {
        allow = pageHistory[pageHistory.length - 1];
        if(allow.id === 'dummy') {
            allow = false;
            break latestCheck;
        }
        allow = !(
            allow.id === id &&
            allow.type === type //check type just in case they somehow have the same id but different type
        );
    }
    if(allow) {
        pageHistory.push(hobj);
    } else {
        pageHistory[pageHistory.length - 1] = hobj; //update the entry instead.
    }
    sessionStorage.setItem('pagehistory',JSON.stringify(pageHistory));
}

function backHistory() {
    var run = true;
    while(run) {
        pageHistory.pop();
        if(pageHistory.length === 0) {
            location = '/home/index.html'; //no need to reset pagehistory here, going to index will reset it.
            return;
        }

        run = pageHistory[pageHistory.length - 1].id === 'dummy';
    }
    sessionStorage.setItem('pagehistory',JSON.stringify(pageHistory));
    var bh = pageHistory[pageHistory.length - 1];
    historyItemOpenSwitch(bh.id, bh.type, bh.name, bh.exdata);
}

function historyItemOpenSwitch(id,type,name,extra) {
    var prms = new URLSearchParams();
    if(typeof(extra) === 'object') {
        var kagi = Object.keys(extra);
        for(var i = 0; i < kagi.length; i++) {
            prms.append(kagi[i], extra[kagi[i]]);
        }
        kagi = undefined; //clean up
    }
    switch(type) {
        case 'video': 
            if('loadAnotherVideo' in window) {
                loadAnotherVideo(id);
            } else {
                prms.append('v',id);
                location = '/watch/index.html#' + prms.toString();
            }
            break;
        case 'search': goToSearchScreen(id); break;
        case 'channel': location = '/channel/index.html#' + encodeURIComponent(id); break;
        case 'playlist': goToSearchScreen(id, 'playlist', name); break;
        case 'userplaylist': goToSearchScreen(id, 'userPlaylist'); break;
        case 'userplaylistlist': goToSearchScreen(null, 'userPlaylistList'); break;
        case 'channelvideos':goToSearchScreen(id, 'channel', name);break;
        case 'channelplaylists':goToSearchScreen(id, 'channelPlaylists', name);break;
        case 'historypage': location = '/history/index.html'; break;
        case 'subscriptionspage': goToSearchScreen(null, 'subscriptions'); break;
        case 'subscriptionsfeed': location = '/subscriptions/index.html'; break;
        case 'settingspage': location = '/settings/index.html'; break;

        default:
            console.error('backHistory: the entry does not have a valid type.'); break;
    }
}

function goToSearchScreen(id,mode,name,extra) {
    var prms = new URLSearchParams();
    prms.append('q',id);

    if(extra instanceof URLSearchParams) {
        for([k,v] of extra) {
            prms.append(k, v);
        }
    } else if(typeof(extra) === 'object') {
        Object.keys(extra).forEach((k)=>{
            prms.append(k, extra[k]);
        });
    }

    if(typeof(mode) === 'string'){prms.append('type',mode);}
    if(typeof(name) === 'string'){prms.append('headertext',name);}
    location = '/search/index.html#' + prms.toString();
}


/* cache */ ///////////////////////////////////////////////////////////////////////////////////////////


var itemCacheUseSessionStorage = false;

function getAllItemCache() {
    var c = (itemCacheUseSessionStorage? sessionStorage : localStorage).getItem('cache');
    if(c) {
        c = JSON.parse(c);
    } else {
        c = {};
    }

    return c;
}

function itemCache(itemType, id, action, data, extraData, dontUpdateTime) {
    if(!getSettingValue('cache-enabled')) {return false;}

    var lastCacheClean = parseInt(localStorage.getItem('last-cache-clean')) || 0, theCurrentTime = (new Date()).getTime();
    if(lastCacheClean + 1000 * 60 * 30 < theCurrentTime) {
        itemCacheClean();
        localStorage.setItem('last-cache-clean', theCurrentTime);
    }

    var c = getAllItemCache();

    /* if(
        [
            'channels',

        ].indexOf(itemType) !== -1
    ) { */

        if(!(itemType in c)) {
            c[itemType] = {};
        }

        switch(action) {
            case 'update':
                c[itemType][id] = {
                    data: data,
                    extraData: extraData,
                    updated: dontUpdateTime? c[itemType][id].updated : ((new Date()).getTime())
                };
                break;
            case 'delete':
                delete c[itemType][id];
                break;
            case 'get': 
                if(id in c[itemType]) {
                    var cd = c[itemType][id];
                    //check if cache is too old
                    if(itemCacheCheckTooOld(cd.updated)) {
                        if(!data) { //data instead will be bool whether to return the entire object instead of just the data
                            cd = cd.data;
                        }
                        return cd;
                    }
                }
                return false;
        }

        itemCacheAllUpdate(c);
    /* } */
}

function itemCacheClean() {
    var ch = getAllItemCache(), 
    topCh = Object.keys(ch),
    delCount = 0;

    for(var i = 0; i < topCh.length; i++) {
        var tc = Object.keys(ch[topCh[i]]);
        for(var j = 0; j < tc.length; j++) {
            var e = ch[topCh[i]][tc[j]];
            //count++;
            if(itemCacheCheckTooOld(e.updated)) {
                delete ch[topCh[i]][tc[j]];
                delCount++;
            }
        }
    }

    itemCacheAllUpdate(ch);
    console.log('cache auto clean: ' + delCount + ' items removed.');
}

function itemCacheCheckTooOld(t) {
    return (new Date()).getTime() - t < 1000 * 60 * 30 /* 30 minutes */
}
function itemCacheAllUpdate(d) {
    (itemCacheUseSessionStorage? sessionStorage : localStorage).setItem('cache', JSON.stringify(d));
}

/* playlists will be here AND also in /main-app/userPlaylist.js AND also in ./userPlaylist.js */
var userPlaylistStore = window.parent.userPlaylistStore, 
userPlaylistModify = window.parent.userPlaylistModify,
userPlaylistManage = window.parent.userPlaylistManage;
function getPlaylistErrors() {return window.parent.playlistErrors}
function userPlaylistMdataCache() {return window.parent.userPlaylistMdataCache}