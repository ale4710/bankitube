var ScrollingText = (function(){
	var scrollingTextList = [],
	scrollingTextAnimFrame = null,
	scrollingTextLastAnimationFrame = null,
	scrollSpeed = 40 * (1 + getSettingValue('text-scroll-speed')),
	scrollResetDelay = 500 * (4 - getSettingValue('text-scroll-speed')); //text-scroll-speed is 3 maximum

	function animateScrollingText() {
		var keepAnimating = false;
		
		var now = performance.now();
		
		if(scrollingTextLastAnimationFrame) {
			var deltaTime = (now - scrollingTextLastAnimationFrame) / 1000;
			if(deltaTime < 500) { //if it froze for half a seconds its kinda strange
				scrollingTextList.forEach((st)=>{
					if(st.playing) {
						keepAnimating = true;
						st.animate(deltaTime);
					}
				});
			} else {
				keepAnimating = true; //it probably just froze, try another frame
			}
		} else {
			keepAnimating = true;
		}

		if(keepAnimating) {
			scrollingTextLastAnimationFrame = performance.now();
			contAST();
		} else {
			scrollingTextAnimFrame = null;
			scrollingTextLastAnimationFrame = null;
		}
	}

	function contAST() {
		//no need to deal with visibilyState and stuff like that
		//requestAnimationFrame deals with that already
		scrollingTextAnimFrame = requestAnimationFrame(animateScrollingText);
	}

	function checkAnimSTPossible() {
		if(scrollingTextAnimFrame === null) {
			contAST();
		}
	}

	function updateSTAList(startIndex) {
		for(var i = startIndex; i < scrollingTextList.length; i++) {
			scrollingTextList[i].stlIndex = i;
		}
	}

	return class ScrollingText {
		constructor(element, container, padding) {
			if(element instanceof HTMLElement) {
				this.element = element;

				//container.
				containerCheck: {
					if(container instanceof HTMLElement) {
						if(container.contains(element)) {
							this.container = container;
							break containerCheck;
						}
					}

					container = null;
				}

				if(!container) {
					container = element.parentElement;
				}

				this.container = container;

				//container's padding.
				this.padding = padding || 0;

				//everything else.
				this.state = 0;
				this.timeout = null;
				this.playing = false;

				this.stlIndex = scrollingTextList.length;
				scrollingTextList.push(this);
			} else {
				throw TypeError('element was not HTMLElement');
			}
		}

		animate(deltaTime) {
			switch(this.state) {
				case 0: //before scroll start
				case 2: //after scroll end
					if(this.timeout === null) {
						this.timeout = setTimeout(
							()=>{
								if(this.state === 2) {
									this.reset();
								} else {
									this.forwardState();
								}
								this.timeout = null;
							},
							scrollResetDelay
						);
					}
					break;
				case 1: //scrolling
					var e = this.element,
					c = this.container;
					if(
						(e.offsetLeft - this.padding) * -1 > 
						e.offsetWidth - c.offsetWidth + (this.padding * 2)
					) {
						this.forwardState();
						return;
					}
					e.style.left = (e.offsetLeft - this.padding - (scrollSpeed * deltaTime)) + 'px';
				break;
			}
		}

		forwardState() {
			//newstate, maxstate
			var ns = this.state + 1, ms = 2;
			if(ns > ms) {
				ns = 0;
			}
			this.state = ns;
		}

		pause() {
			this.playing = false;
			this.reset();
			//console.log(this, 'paused');
		}

		play() {
			this.reset();
			this.playing = true;
			checkAnimSTPossible();
			//console.log(this, 'play');
		}

		reset() {
			clearTimeout(this.timeout);
			this.timeout = null;
			this.element.style.left = null;
			this.state = 0;
		}

		remove() {
			this.pause();
			scrollingTextList.splice(this.stlIndex, 1);
			updateSTAList(this.stlIndex);
		}
	}
})();