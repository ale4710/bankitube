var getRandomClassName = (function(){
	var takennames = [];
	return function() {
		var name;
		while(
			takennames.indexOf(name) !== -1 ||
			typeof(name) === 'undefined'
		) {
			name = Math.floor(Math.random() * Math.pow(10, 20));
		}
		takennames.push(name);
		return name;
	}
})();

class Menu {
    constructor(container) {
        this.container = container;

        this.menu = document.createElement('div');
        this.menu.classList.add('menu');

        this.container.appendChild(this.menu);
		
		this.className = `menu-${getRandomClassName()}`;
    }
	
	getChildren() {
		return this.menu.querySelectorAll(`.${this.className}.focusable-item`);
		//return this.menu.getElementsByClassName(this.className);
	}

    clearMenu() {
        var mc = this.menu.getElementsByClassName(this.className);
		while(mc.length !== 0) {mc[0].remove()}
    }

    navigate(d) {
        var c = this.getChildren();
        if(d === 0) {
            c[0].focus();
        } else {
            navigatelist(
                actEl().tabIndex,
                c, d
            );
        }
    }

    addOption(label, id) {
        var opt = document.createElement('div');
        opt.classList.add('menu-entry', 'focusable-item', this.className);
        opt.tabIndex = this.getChildren().length;
        opt.textContent = label || '';
        if(typeof(id) !== 'undefined'){opt.dataset.id = id;}
        this.menu.appendChild(opt);
        return opt;
    }
	
	addSeperator(label) {
        var opt = document.createElement('div');
        opt.classList.add('menu-entry', 'menu-seperator', this.className);
        opt.textContent = label || '';
        this.menu.appendChild(opt);
        return opt;
	}
}

class OptionsMenu extends Menu {
    constructor(headerLabel) {
        super(document.createElement('div'));
        this.container.classList.add('option-menu-cont');
        this.menu.classList.add('option-menu');

        this.allowBackCV = null;

        this.screen = document.createElement('div');
        this.screen.classList.add('screen','dim','hidden','option-menu-screen');

        this.header = document.createElement('div');
        this.header.classList.add('menuheader');
        this.header.textContent = headerLabel;

        this.screen.appendChild(this.container);
        this.container.insertBefore(
            this.header,
            this.menu
        );
        //this.container.appendChild(this.header);
        document.body.appendChild(this.screen);
    }

    updateHeader(headerLabel) {
        this.header.textContent = headerLabel;
    }

    menuViewToggle(v,f,fn) {
        //v = visible (bool), f = focus mode (bool) when v=true, fn = focus number when f=true
        if(v) {
            //this 'optionsMenuShowing' thing might be useless. i'll remove it if i never ever use it.
            document.body.classList.add('optionsMenuShowing');
            this.screen.classList.remove('hidden');
            this.allowBackCV = allowBack;
            allowBack = false;
            if(f) {
                if(isNaN(fn)) {fn = 0;}
                if(fn in this.menu.children) {
                    this.menu.children[fn].focus();
                }
            }
        } else {
            document.body.classList.remove('optionsMenuShowing');
            this.screen.classList.add('hidden');
            if(typeof(this.allowBackCV) === 'boolean') {allowBack = this.allowBackCV;}
            this.allowBackCV = null;
        }
    }
}

class OptionsMenuSelectable extends OptionsMenu {
    constructor(headerLabel, type) {
        super(headerLabel);

        this.validTypes = [
            'radio',
            'checkbox',
            'text',
            'tel'
        ];
    
        this.textTypes = [
            'text',
            'tel'
        ];

        if(!this.changeType(type)) {
            throw TypeError(`Type "${type}" is not valid.`);
        }
        this.inputType = type;
        this.groupName = getRandomClassName();
    }

    selectItem(n) {
        //n = actEl() tabindex?
        if(this.textTypes.indexOf(this.inputType) === -1) { //not a text-type
            var tc = this.menu.children;
            if(n in tc) {
                var ie = tc[n].children[0];
                switch(this.inputType) {
                    case 'radio':
                        ie.checked = true;
                        break;
                    case 'checkbox':
                        ie.checked = !ie.checked;
                        break;
                }
            }
        }
    }

    getValue() {
        if(this.menu.children.length === 0) {return false;}

        var e,s,
        cb = ()=>{return this.menu.querySelectorAll('input:checked')};

        switch(this.inputType) {
            case 'radio':
                e = cb()[0];
                if(e) {
                    s = e.parentElement.tabIndex;
                } else {
                    e = null;
                    s = null;
                }
                break;
            case 'checkbox':
                e = cb();
                s = [];
                for(var i = 0; i < e.length; i++) {
                    s.push(e[i].parentElement.tabIndex);
                }
                break;
            case 'text':
            case 'tel':
                e = this.menu.children[0];
                s = e.value;
                break;
        }

        return {
            value: s,
            element: e
        }
    }

    setValue(indiciesOrValue, value) {
        switch(this.inputType) {
            case 'radio':
                this.menu.children[indiciesOrValue].checked = !!value;
                break;
            case 'checkbox':
                if(
                    Array.isArray(indiciesOrValue) &&
                    Array.isArray(value)
                ) {
                    indiciesOrValue.forEach((menuIndex, arrayIndex)=>{
                        this.menu.children[menuIndex].checked = !!value[arrayIndex];
                    });
                } else {
                    throw TypeError(`both parameters passed to setValue on a checkbox ${OptionsMenuSelectable.name} needs to be an array of indicies.`);
                }
                break;
            case 'text':
            case 'tel':
                this.menu.children[0].value = indiciesOrValue;
                break;
        }
    }

    checkValidType(t) {
        return this.validTypes.indexOf(t) !== -1;
    }
    checkTextType(t) {
        return this.textTypes.indexOf(t) !== -1;
    }

    changeType(ntype) {
        if(this.checkValidType(ntype)) {
            this.clearMenu();
            this.inputType = ntype;
            if(this.checkTextType(ntype)) {
                this.addOption('');
            }
            return true;
        } else {
            return false;
        }
        
    }

    menuViewToggle(v,ft,n) {
        //v = visible? (true/false)
        //ft = focus type. 0 = nah, 1 = manual focus, 2 = focus on selected (depending on type)
        //n = which one to focus to (only works when ft = 1)
        if(v) {
            if(ft === 2) {
                switch(this.inputType) {
                    case 'radio':
                        var selel = this.menu.querySelector('input:checked');
                        if(selel) {
                            n = selel.parentElement.tabIndex;
                        }
                        break;
                    case 'text':
                    case 'tel':
                        focusInput(this.menu.children[0]);
                        ft = true;
                        break;
                }
            }
        }

        super.menuViewToggle(v,ft,n);
    }

    navigate(d) {
        if(!this.checkTextType(this.inputType)) {
            //is not text type
            super.navigate(d);
        }
    }

    addOption(label, selected) {
        var input = document.createElement('input');
        input.type = this.inputType;

        if(this.checkTextType(this.inputType)) {
            if(this.menu.children.length === 0) {
                input.tabIndex = 0;
				input.classList.add(this.className);
                this.menu.appendChild(input);
                return input;
            } else {
                return null;
            }
        }

        var opt = super.addOption(label);
        
        input.name = this.groupName;
        input.checked = !!selected;

        opt.appendChild(input);

        var inputDisp = document.createElement('span');
        inputDisp.classList.add('vertical-center');
        opt.appendChild(inputDisp);
        return opt;
    }
}

class Tabs {
    constructor(appendToBody, existingContainer) {
        if(existingContainer) {
            this.container = existingContainer;
        } else {
            this.container = document.createElement('div');
        }
        this.container.classList.add('tabstrip-cont');

        this.tabStrip = document.createElement('div');
        this.tabStrip.classList.add('tabstrip');

        this.container.appendChild(this.tabStrip);

        this.tabs = {
            byNumber: [],
            byId: {}
        }

        if(appendToBody) {
            document.body.appendChild(this.container);
        }

        this.errors = {
            unsupportedType: TypeError('you passed an unsupported type to removeTab(). Expected string or number.')
        }
    }

    addTab(label,id) {
        var ct = document.createElement('div');
        ct.classList.add('tabstrip-tab');
        ct.textContent = label;
        if(!id) {
            var l = true;
            while(id in this.tabs.byId || l) {
                id = Math.random().toString();
                l = false;
            }
        }
        this.tabs.byId[id] = {
            element: ct,
            index: id
        };
        this.tabs.byNumber.push({
            element: ct,
            id: id
        });

        this.tabStrip.appendChild(ct);
    }

    removeTab(id) {
        switch(typeof(id)) {
            case 'string': 
                if(id in this.tabs.byId) {
                    var ct = this.tabs.byId[id], 
                    ix = this.tabs.byId[id].index;
                    ct.element.remove();
                    delete this.tabs.byId[id];
                    this.removeTab(ix);
                }
                break;
            case 'number': 
                if(id in this.tabs.byNumber) {
                    var ri = this.tabs.byNumber.splice(id,1)[0];
                    ri.remove();
                    this.removeTab(ri.id);
                }
                break;
            default: throw this.errors.unsupportedType;
        }
    }

    focusTab(id) {
        this.tabs.byNumber.forEach((t)=>{
            t.element.classList.remove('active');
        });

        var activeTab;

        switch(typeof(id)) {
            case 'string': activeTab = this.tabs.byId; break;
            case 'number': activeTab = this.tabs.byNumber; break;
            default: throw this.errors.unsupportedType;
        }

        activeTab = activeTab[id].element;
        activeTab.classList.add('active');

        this.tabStrip.scrollTo(
            activeTab.offsetLeft + (activeTab.offsetWidth / 2) - 
            (this.tabStrip.offsetWidth / 2)
        ,0);
    }
}

var optionsMenuMultiSliders = (function(){
	var interface = {};
	
	interface.genericKeyHandler = (function(
		ms, 
		updFn = emptyfn, 
		allFn = emptyfn
	){
		return (function(k){
			switch(k.key) {
				case 'ArrowUp':
					var u = -1;
				case 'ArrowDown':
					ms.navigate(u || 1);
					break;
					
				case 'ArrowRight':
					var up = true;
				case 'ArrowLeft':
					var aeti = actEl().tabIndex,
					wv = actEl().dataset.id;
					updFn(
						((wv === null)? aeti : wv),
						ms.updateValue(
							!!up,
							aeti
						)
					);
					break;
			}
			
			allFn(k);
		});
	});
	
	interface.OptionsMenuMultiSliders = (class OptionsMenuMultiSliders extends OptionsMenu {
		constructor(headerLabel) {
			super(headerLabel);
			this.groupName = getRandomClassName();
		}

		addOption(
			label, 
			id, 
			min, 
			max, 
			step = 1, 
			defVal
		) {
			var el = super.addOption(label, id),
			ra = document.createElement('input'),
			lb = document.createElement('div');

			ra.className = 'options-menu-range';
			ra.type = 'range';
			ra.min = min;
			ra.max = max;
			ra.step = step;
			if(typeof(defVal) === 'number') {
				ra.value = defVal;
			} else {
				var rngHalf = (max - min) / 2;
				ra.value = min + (rngHalf - (rngHalf % step));
			}
			
			ra.dataset.default = ra.value;

			lb.className = 'menu-entry-right-text';
			lb.textContent = ra.value;

			el.appendChild(lb);
			el.appendChild(ra);

			return el;
		}

		getInnerElements(which) {
			var te;
			switch(typeof(which)) {
				case 'number':
					te = this.getChildren()[which];
					break;
				case 'string':
					var l = this.getChildren();
					for(var i = 0; i < l.length; i++) {
						if(l[i].dataset.id === which) {
							te = l[i];
							break;
						}
					}
					break;
				default:
					throw 'please pass number or string to getValue.';
			}

			if(te) {
				return {
					range: te.getElementsByClassName('options-menu-range')[0],
					label: te.getElementsByClassName('menu-entry-right-text')[0]
				};
			} else {
				return null;
			}
		}
		
		resetValue(which) {
			var ie = this.getInnerElements(which);
			if(ie) {
				this.updateValue(
					parseFloat(ie.range.dataset.default),
					which,
					true
				);
			}
		}
		
		resetAll() {
			var l = this.getChildren().length;
			while(l > 0) {
				this.resetValue(l - 1);
				l--;
			}
		}

		updateValue(incr, which, abs) {
			if(typeof(which) !== 'number') {
				which = actEl().tabIndex;
			}

			var els = this.getInnerElements(which);
			if(els) {
				var r = els.range;
				if(!!abs) {
					r.value = incr;
				} else {
					if(!!incr) {
						r.stepUp();
					} else {
						r.stepDown();
					}
				}
				els.label.textContent = r.value;
				return parseFloat(r.value);
			}
			return null;
		}

		getValue(which) {
			var e = this.getInnerElements(which);
			if(e) {e = parseFloat(e.range.value);}
			return e;
		}
	});
	
	return interface;
})();

class PreviousFocusHandler {
    constructor(){
        this.clear();
    }

    save(hideCallback) {
        this.lastEl = actEl();
        this.lastPage = curpage;
        if(typeof(hideCallback) === 'function') {
            this.hideCallback = hideCallback;
        }
    }

    refocus() {
        if(this.lastEl) {this.lastEl.focus()}
    }
    loadpage() {
        if(this.lastPage !== null) {curpage = this.lastPage}
    }
    execcallback() {
        if(typeof(this.hideCallback) === 'function') {
            this.hideCallback(...arguments);
        }
    }
	
	loadall() {
		this.loadpage();
		this.refocus();
	}

    clear() {
        this.lastEl = null;
        this.lastPage = null;
        this.hideCallback = null;
    }
}