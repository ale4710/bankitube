function addGlobalReference(type, filename) {
	return new Promise(function(resolve, reject){
		var s = document.createElement([
			'script',
			'link'
		][type]);

		switch(type) {
			case 0: //script
				s.src = filename + '.js';
				s.defer = true;
				s.async = false;
				break;
			case 1: //style
				s.rel = 'stylesheet';
				s.type = 'text/css';
				s.href = filename + '.css';
				break;
		}
		
		s.addEventListener('load', resolve);
		s.addEventListener('error', reject);

		document.head.appendChild(s);
	});
}
