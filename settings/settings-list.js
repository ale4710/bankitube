var 
settingsList = {
    /* 
    'setting-name': {
        'label': string,
        //is just the label. it is different from setting-name.

        'values': ['setting value 1', 'setting value 2', 'etc'],
        //values is what will be displayed, when type is not [ 4 ], [ 2 ], or [ 3 ].

        'type': number,
        //type of setting.
        // 0 = select 1 setting only (radio)
        // 1 = select multiple (checkbox)
        // 2 = number input
        // 3 = text input
        // 4 = excecute function
        // 5 = go to settings page

        'action': () => {} or string,
        //when type is [ 4 ] it will excecute the function. should be function.
        //when type is [ 5 ] it will go to that setting category. should be string.
        //any other type, it will excecute the function after it changes the setting.

        'default': number or string,
        //default setting

        'check': function,
        //used for text input, to check for valid input.

        'help': string,
        //used for the "help" action. shows what the setting will do, or something.
    }
    */

    'invidious-instance': {
        type: 3,
        default: 'https://youtube.076.ne.jp',
        check: (s)=>{return urlRegex.test(s);},
    },

    'color-theme': {
        type: 0,
        default: 0,
        values: 2,
        action: updateTheme
    },

    'language': {
        type: 0,
        default: 0,
        values: 7,
        action: ()=>{
            messageBox(
                i18n('are-you-sure'),
                i18n('settings-confirm-language'),
                {
                    right: messageBoxOption(
                        ()=>{
                            window.parent.fullReload();
                        },
                        i18n('yes')
                    ),
                    left: messageBoxOption(
                        messageBoxDefaultBackCallback,
                        i18n('no')
                    )
                }
            );
        }
    },

    'homescreen-top-display-mode': {
        type: 1,
        default: '[0]',
        values: 3
    },
    
    'show-version-on-homemenu': {
        type: 0,
        default: 0,
        values: 2
    },

    'video-autoplay': {
        type: 0,
        default: 1,
        values: 3
    },

    'video-time-display': {
        type: 0,
        default: 0,
        values: 3
    },

    'video-seek-time': {
        type: 0,
        default: 1,
        values: 4
    },

    'use-format-streams': {
        type: 0,
        default: 0,
        values: 2
    },

    'default-video-quality': {
        type: 0,
        default: 1,
        values: 4
    },

    'text-scroll-speed': {
        type: 0,
        default: 1,
        values: 4
    },

    'default-list-view': {
        type: 0,
        default: 0,
        values: 2
    },

    'comments-user-images': {
        type: 0,
        default: 1,
        values: 2
    },

    'sekibanki-loading-icon': {
        type: 0,
        default: 1,
        values: 2
    },

    'random-include-categories': {
        type: 1,
        default: '[0,1,2,3,4,5]',
        values: 6
    },

    'random-include-mature': {
        type: 0,
        default: 0,
        values: 2
    },

    'sponsorblock-categories': {
        type: 1,
        default: '[]',
        values: 7
    },

    'sponsorblock-instance': {
        type: 3,
        default: 'https://sponsor.ajay.app',
        check: (s)=>{return urlRegex.test(s);},
    },

    'segment-auto-skip': {
        type: 0,
        default: 1,
        values: 2
    },

    'record-history': {
        type: 0,
        default: 1,
        values: 2
    },

    'listen-mode-show-listen-mode-on-thumbnail': {
        type: 0,
        default: 1,
        values: 2
    },

    'listen-mode-blurry-thumbnail': {
        type: 0,
        default: 1,
        values: 2
    },

    'video-always-show-time': {
        type: 0,
        default: 0,
        values: 2
    },

    'video-hours-time-display': {
        type: 0,
        default: 1,
        values: 2
    },

    'date-date-format': {
        type: 0,
        default: 0,
        values: 10,
        action: ()=>{window.parent.localizationUpdateObjects();}
    },

    'date-time-format': {
        type: 0,
        default: 6,
        values: 13,
        action: ()=>{window.parent.localizationUpdateObjects();}
    },

    'delete-all-history': {
        type: 4,
        action: ()=>{
            doubleConfirmAction(
                i18n('settings-confirm-delete-all-history'), 
                ()=>{
                    window.parent.allPageHistory.clear().then(()=>{
                        alertMessage(i18n('settings-done-delete-all-history'),5000,0);
                    }).catch((e)=>{
                        console.error(e);
                        alertMessage(i18n('settings-fail-delete-all-history'),5000,3);
                    });
                }
            );
        },
    },

    'delete-all-subscriptions': {
        type: 4,
        action: ()=>{
            doubleConfirmAction(
                i18n('settings-confirm-delete-all-subscriptions'),
                ()=>{
                    channelSubscriptions.clear().then(()=>{
                        alertMessage(i18n('settings-done-delete-all-subscriptions'),5000,0);
                    });
                }
            );
        },
    },

    'goto-import-export': {
        type: 4,
        action: ()=>{location = '/settings/imexport/index.html'}
    },

    'cache-enabled': {
        type: 0,
        default: 1,
        values: 2
    },

    'clear-cache': {
        type: 4,
        action: ()=>{
            localStorage.removeItem('cache');
            alertMessage(i18n('settings-done-clear-cache'), 5000, 1);
        },
    },

    'notification-check-while-app-running': {
        type: 0,
        default: 0,
        values: 2
    },

    'notification-enabled': {
        type: 0,
        default: 0,
        values: 2,
        action: window.parent.notifToggle
    },

    'notification-checking-subscriptions': {
        type: 0,
        default: 0,
        values: 2
    },

    'notification-check-interval': {
        type: 0,
        default: 5,
        values: 9
    },

    'captions-size': {
        type: 0,
        default: 1,
        values: 4
    },

    'captions-text-color': {
        type: 0,
        default: 0,
        values: 6
    },

    'captions-text-edge-style': {
        type: 0,
        default: 0,
        values: 3
    },

    'captions-background-color': {
        type: 0,
        default: 1,
        values: 6
    },

    'captions-background-opacity': {
        type: 0,
        default: 3,
        values: 5
    },

    'notification-icon': {
        type: 0,
        default: 0,
        values: 4
    },

    'notification-display-style': {
        type: 0,
        default: 0,
        values: 2
    },

    'notification-preview': {
        type: 4,
        action: ()=>{
            new Notification(
                i18n('notification-text-title'), 
                {
                    body: i18n('notification-text-body', 3, {n0: 'Sekibanki', n1: 'Sakuya', n2: 'Kaguya'}),
                    requireInteraction: getSettingValue('notification-display-style') === 1,
                    icon: [
                        '/img/icon/112.png',
                        '/img/bankihead.png',
                        '/img/channel-with-shadow.png',
                        ''
                    ][getSettingValue('notification-icon')]
                }
            );
        }
    },

    'notify-playlist-end': {
        type: 0,
        default: 0,
        values: 2
    },

    'alt-rating-data-enabled': {
        type: 0,
        default: 0,
        values: 2
    },

    'alt-rating-data-instance': {
        type: 3,
        default: 'https://returnyoutubedislikeapi.com',
        check: (s)=>{return urlRegex.test(s);}
    },
	
	'hide-related-videos': {
        type: 0,
        default: 0,
        values: 2
    },

    'about': {
        type: 4,
        action: ()=>{window.open('/about/about.html');}
    },

    'category-general': {
        action: 'general',
        type: 5
    },
    'category-locale': {
        action: 'locale',
        type: 5
    },
    'category-videos': {
        action: 'videos',
        type: 5
    },
    'category-data': {
        action: 'data',
        type: 5
    },
    'category-external-services': {
        action: 'external-services',
        type: 5
    },
    'category-segment-skipping': {
        action: 'segment-skipping',
        type: 5
    },
    'category-alt-rating-data': {
        action: 'alt-rating-data',
        type: 5
    },
    'category-display': {
        action: 'display',
        type: 5
    },
    'category-captions': {
        action: 'captions',
        type: 5
    },
    'category-notification': {
        action: 'notification',
        type: 5
    },
    'category-subscriptions-notification': {
        action: 'subscriptions-notification',
        type: 5
    }
},

settingsListCategories = {

    //label is the label of the category.
    //settings is an array with the names from above. they will be displayed in this order.

    'default': {
        label: 'Settings',
        settings: [
            'category-general',
            'category-locale',
            'category-display',
            'category-videos',
            'category-notification',
            'category-external-services',
            'category-data',
            'about'
        ]
    },

    'locale': {
        settings: [
            'language',
            'date-date-format',
            'date-time-format'
        ]
    },

    'display': {
        settings: [
            'category-captions',
            'color-theme',
            'default-list-view',
			'hide-related-videos',
            'comments-user-images',
            'video-always-show-time',
            'video-time-display',
            'video-hours-time-display',
            'listen-mode-show-listen-mode-on-thumbnail',
            'listen-mode-blurry-thumbnail',
            'sekibanki-loading-icon',
        ]
    },

    'captions': {
        settings: [
            'captions-size',
            'captions-text-color',
            'captions-background-color',
            'captions-background-opacity',
            'captions-text-edge-style',

        ]
    },

    'general': {
        settings: [
            'invidious-instance',
            'homescreen-top-display-mode',
            'show-version-on-homemenu',
            'text-scroll-speed',
            'random-include-categories',
            'random-include-mature'
        ]
    },

    'videos': {
        settings: [
            'video-autoplay',
            'video-seek-time',
            'use-format-streams',
            'default-video-quality',
        ]
    },

    'notification': {
        settings: [
            'category-subscriptions-notification',
            'notify-playlist-end',
        ]
    },

    'subscriptions-notification': {
        settings: [
            'notification-enabled',
            'notification-preview',
            'notification-icon',
            'notification-display-style',
            'notification-check-interval',
            'notification-check-while-app-running',
            'notification-checking-subscriptions'
        ]
    },

    'external-services': {
        settings: [
            'category-segment-skipping',
            'category-alt-rating-data'
        ]
    },

    'segment-skipping': {
        settings: [
            'sponsorblock-categories',
            'sponsorblock-instance',
            'segment-auto-skip'
        ]
    },

    'alt-rating-data': {
        settings: [
            'alt-rating-data-enabled',
            'alt-rating-data-instance'
        ]
    },

    'data': {
        settings: [
            'goto-import-export',
            'record-history',
            'delete-all-history',
            'delete-all-subscriptions',
            'cache-enabled',
            'clear-cache'
        ]
    }
}
;
