function importSubscriptionsYoutube(r) {
    var rr = pJSON(r), f = 0, s = 0;
    if(Array.isArray(rr)) {
        progressDisplayVisToggle(true);
        var pd = new ProgressDisplayProgBars(i18n('settings-file-import-subscriptions') + ' ');
        channelSubscriptions.clear().then(()=>{
            var ce = -1,
            nextImport = ()=>{
                ce++;
                pd.updatePercent(ce, rr.length - 1);
                if(ce === rr.length) {
                    importSubscriptionsDone(s,f);
                } else {
                    var e = rr[ce];
                    try {
                        subscriptionsAdd(
                            e.snippet.resourceId.channelId,
                            e.snippet.title,
                            e.snippet.thumbnails.default.url,
                            true,
                            ()=>{
                                s++;
                                nextImport();
                            }
                        );
                    } catch(e) {
                        f++;
                        nextImport();
                    }
                    
                }
            };
    
            nextImport();
        });
        
    } else {
        alertMessage(i18n('settings-file-import-failed'),5000,3);
    }
}

function importSubscriptionsNewpipe(r) {
    
    var rr = pJSON(r), f = 0, s = 0, fail = ()=>{
        alertMessage(i18n('settings-file-parse-failed'),5000,3);
    };
    if(rr && 'subscriptions' in rr) {
        rr = rr.subscriptions;
        if(Array.isArray(rr)) {
            progressDisplayVisToggle(true);
            var pd = new ProgressDisplayProgBars(i18n('settings-file-import-subscriptions') + ' ');
            channelSubscriptions.clear().then(()=>{
                var ce = -1, nextImport = ()=>{
                    ce++;
                    pd.updatePercent(ce, rr.length - 1);
                    if(ce === rr.length) {
                        importSubscriptionsDone(s,f);
                    } else {
                        var e = rr[ce];
                        try {
                            var id = e.url.substr('https://www.youtube.com/channel/'.length);
                            if(!subscriptionsCheck(id)) {
                                subscriptionsAdd(
                                    id,
                                    e.name,
                                    null,
                                    true,
                                    ()=>{
                                        s++;
                                        nextImport();
                                    }
                                );
                            }
                        } catch(e) {
                            f++;
                            nextImport();
                        }
                    }
                };
                nextImport();
            });
        } else {
            fail();
        }
    } else {
        fail();
    }
}

function importSubscriptionsDone(s,f) {
    var m = i18n('settings-subscription-import-count', s);
    if(f !== 0) {
        m += i18n('settings-subscription-import-fail-count', f);
    }

    alertMessage(m,5000,0);
    settingsDataIEcancel();
}

