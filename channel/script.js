var channelId, 
channelName, 
channelUrl, 
subscriptionsProcessing = false,

channelMenu = new Menu(eid('channel-options')),
channelMenuOptId = {
    video: '0',
    playlist: '1',
    desc: '2',
    sub: '3',
    share: '4'
}
;

window.addEventListener('DOMContentLoaded',()=>{
    channelId = location.hash.substr(1);
    pushHistory(channelId,'channel',channelId);
    pushToAllHistory(channelId,'channel');

    //create menu items
    [
        {a: channelMenuOptId.video, t: 'video-menu-item'},
        {a: channelMenuOptId.playlist, t: 'playlists-menu-item'},
        {a: channelMenuOptId.desc, t: 'description'},
        {a: channelMenuOptId.sub, h: '<span id="channel-subscribe"></span>'},
        {a: channelMenuOptId.share, t: 'share-action'}
    ].forEach((entry)=>{
        var entryEl = channelMenu.addOption(i18n(entry.t));
        if('h' in entry) {entryEl.innerHTML = entry.h}
        entryEl.dataset.action = entry.a;
    });


    var cachedData = itemCache('channels', channelId, 'get');
    if(cachedData) {
        displayChannel(cachedData);
    } else {
        requestItem(
            'channels',
            channelId,
            '',
            (r)=>{
                itemCache('channels', channelId, 'update', r);
                displayChannel(r);
            },  
            null,
            true,
            true
        );
    }

    subscriptionsCheck(channelId, (c)=>{
        var tn = 'subscribe-action';
        if(c){tn = 'un' + tn}
        eid('channel-subscribe').textContent = i18n(tn);
    });
});


function displayChannel(rqResp){
    console.log('display channel.');
    eid('load').classList.add('hidden');
    eid('main').classList.remove('hidden');
    disableControls = false;

    var channelPfp = imgChoose(rqResp.authorThumbnails,'width',100);

    eid('channel-banner').src = imgChoose(rqResp.authorBanners,'width',1000) || '/img/nochannelbanner.png';
    eid('channel-picture').src = channelPfp || '/img/nouserimage-colored.png';
    eid('channel-name').textContent = rqResp.author;
    eid('channel-banner-name').textContent = rqResp.author;
    eid('channel-subcount').textContent = i18n('subscriber-shortened-count', rqResp.subCount, {count: commaSeparateNumber(rqResp.subCount)});
    //eid('channel-subcount').textContent = commaSeparateNumber(rqResp.subCount);
    eid('channel-shortinfo').textContent = i18n(
        'channel-view-join-data', rqResp.totalViews, {
            viewCount: commaSeparateNumber(rqResp.totalViews),
            joinDate: unixdateformat(rqResp.joined)
        }
    );
    //eid('channel-viewcount').textContent = commaSeparateNumber(rqResp.totalViews);
    //eid('channel-joined').textContent = unixdateformat(rqResp.joined);
    eid('channel-description').innerHTML = rqResp.descriptionHtml;

    subscriptionsUpdateMdata(
        rqResp.authorId,
        rqResp.author,
        channelPfp
    );

    channelName = rqResp.author;
    pushHistory(channelId,'channel',channelName);
    //pushToAllHistory(channelId,'channel',channelName);
    mdataStoreSet(
        channelId,
        {
            name: rqResp.author,
            type: 'channel',
            image: channelPfp
        }
    )

    channelUrl = rqResp.authorUrl;


    channelMenu.navigate(0);
    updatenavbar();
}

function selectItem() {
    switch(actEl().dataset.action) {
        case channelMenuOptId.video: goToSearchScreen(channelId, 'channel', channelName); break;
        case channelMenuOptId.playlist: goToSearchScreen(channelId, 'channelPlaylists', channelName); break;
        case channelMenuOptId.sub: 
            if(!subscriptionsProcessing) {
                subscriptionsProcessing = true;
                subscriptionsCheck(channelId, (s)=>{
                    var dfn = ()=>{
                        subscriptionsProcessing = false;
                        var u = '';
                        if(!s){u='un'}
                        eid('channel-subscribe').textContent = i18n(`${u}subscribe-action`);
                    };
                    if(s) {
                        subscriptionsRemove(channelId, false, dfn);
                    } else {
                        subscriptionsAdd(
                            channelId,
                            channelName,
                            eid('channel-picture').src,
                            false,
                            dfn
                        );
                    }
                });
            }
            
            break;
        case channelMenuOptId.desc: showDescription(true); break;
        case channelMenuOptId.share: shareot(linkUtil.formYtLink(channelUrl)); break;
    }
}

/* the function goToSearchScreen is now in /common/js/data.js */

var showDescriptionLastFocus;
function showDescription(tr) {
    if(tr) {
        showDescriptionLastFocus = actEl();
        eid('channel-description-cont').focus();
        eid('channel-description-cont').scrollTo(0,0);
        document.body.classList.add('descshow');
        allowBack = false;
    } else {
        showDescriptionLastFocus.focus();
        showDescriptionLastFocus = undefined;
        document.body.classList.remove('descshow');
        allowBack = true;
    }
}

addPage(
	//key fn
	(function(k){
		var df = actEl().id === 'channel-description-cont';
		switch(k.key) {
			case 'Backspace': if(df){showDescription(false);} break;
			case 'SoftLeft': navMainMenu(0); break;
			case 'ArrowUp':
				var u = -1;
			case 'ArrowDown':
				if(!df){channelMenu.navigate(u || 1);}
				break;
			case 'Enter': if(!df) { selectItem(); }break;

		}
	}),
	//navbar fn
	(function(){
		outputNavbar(
			i18n('menu'),
			actEl().id === 'channel-description-cont'? '' : i18n('select'),
			''
		)
	})
);
