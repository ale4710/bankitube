var noSubscriptions = false, curpage = 0;

window.addEventListener('DOMContentLoaded',()=>{
    pushHistory('subscriptionsfeed');

    channelSubscriptions.length().then((length)=>{
        if(length === 0) {
            noSubscriptionsMessage();
            pageInited();
        } else {
            var chans = [];
            channelSubscriptions.iterate((v,i)=>{
                mdataStore.getItem(i).then((r)=>{
                    if(r === null) {
                        r = {};
                        r.name = makeUnknownTitleItem(i);
                    }
                    r.id = i;
                    chans.push(r);
                    if(chans.length === length) {
                        chans.sort((a,b)=>{
                            return a.name.toLowerCase() > b.name.toLowerCase();
                        });
                        chans.forEach((e,i)=>{
                            var container = document.createElement('div'),
                            name = document.createElement('div'),
                            img = document.createElement('img');
                
                            container.tabIndex = i;
                            container.dataset.id = e.id;
                            container.classList.add('subscription', 'focusable-item');
                
                            name.textContent = e.name;
                            name.classList.add('subscription-name', 'vertical-center');
                
                            img.src = e.image || '/img/nouserimage.png';
                            img.classList.add('subscription-img');
                
                            container.appendChild(img);
                            container.appendChild(name);
                
                            eid('subscriptions-list').appendChild(container);
                
                            if(i === 0) {container.focus();}
                        });

                        pageInited();
                    }
                })
            })
        }
    });
});

function pageInited() {
    disableControls = false;
                        allowBack = true;
                        updatenavbar();
}

function removeCurrentItem() {
    if(!noSubscriptions) {
        var channelName = actEl().getElementsByClassName('subscription-name')[0].textContent,
        fe = actEl();
        messageBox(
            i18n('are-you-sure'),
            i18n('subscription-remove-confirm', {name: channelName}),
            {
                center: messageBoxOption(
                    ()=>{
                        var li = fe.tabIndex;
                        subscriptionsRemove(fe.dataset.id,true);
                        fe.remove();
                        alertMessage(i18n('subscription-removed-named', {name: channelName}),5000,0);
                        var dsl = ecls('subscription');
                        if(dsl.length === 0) {
                            noSubscriptionsMessage();
                        } else {
                            if(dsl.length === li) {
                                dsl[li - 1].focus();
                            } else {
                                for(var i = li; i < dsl.length; i++) {
                                    dsl[i].tabIndex = i;
                                }
                                dsl[li].focus();
                            }
                        }
                    },
                    i18n('yes')
                ),
                left: messageBoxOption(
                    messageBoxDefaultBackCallback,
                    i18n('no')
                )
            }
        );
    }
}

function noSubscriptionsMessage() {
    var ns = document.createElement('center');
    ns.classList.add('center');
    ns.textContent = i18n('subscription-none');
    eid('subscriptions-list').appendChild(ns);
    noSubscriptions = true;
}

function keyHandler(k) {
    if(keyisnav(k)) {k.preventDefault();}
    switch(k.key) {
        case 'SoftLeft': navMainMenu(0); break;
        case 'SoftRight': removeCurrentItem(); break;
        case 'Enter': 
            if(!noSubscriptions) {
                location = '/channel/index.html#' + encodeURIComponent(actEl().dataset.id); 
            }
            break;
        case 'ArrowUp': 
            var u = -1;
        case 'ArrowDown': 
            if(!noSubscriptions) {
                navigatelist(
                    actEl().tabIndex,
                    ecls('subscription'),
                    u || 1
                );
            }
            break;
    }
}
function localupdatenavbar() {
    if(!noSubscriptions) {
        outputNavbar(
            i18n('menu'),
            i18n('view'),
            i18n('remove')
        );
    } else {
        outputNavbar(
            i18n('menu')
        );
    }
}