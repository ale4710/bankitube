var curpage = 0,
/* 
0 - main screen
1 - description
2 - video actions
3 - related videos
4 - captions menu
5 - comments?
6 - overflow video actions
7 - quality selector
8 - speed control menu
*/
videoSeekAmount = [5,10,15,30][getSettingValue('video-seek-time')];

function keyHandler(k) {
    switch(curpage) {
        case 0: mainScreenK(k); break;
        case 1: descriptionK(k); break;
        case 2: actionK(k); break;
        case 3: relatedK(k); break;
        case 4: captionK(k); break;
        case 5: commentsSubMenuK(k); break;
        case 6: overflowVideoActionsK(k); break;
        case 7: qualityMenuK(k); break;
		case 8: speedControl.keyhandler(k); break;
    }
    //global, runs no matter what
    switch(k.key) {
        case 'ArrowUp':
        case 'ArrowDown':
        case 'ArrowLeft':
        case 'ArrowRight':
            if([1,5].indexOf(curpage) === -1) {
                k.preventDefault();
            }
        
    }
}

function mainScreenK(k) {
    switch(k.key) {
        case 'Enter':
            if(videoAbleToPlay) {
                clearTimeout(audioResyncTimeout);
                var v = output.video;
                if(v.paused){
                    v.play();
                }else{
                    v.pause();
                }
            }
            break;
        case '2': fsToggle(); break;
        case '3': 
            var f = true;
        case '1':
            playlistNextVideo(!!f, true); 
            break;
        case '4': toggleVideoLoop(); break;
        case '5': toggleVideoFit(); break;
        case '6': skipSegmentManual(); break;
        case '9': toggleSegmentSkipping(); break;
        case 'ArrowRight': 
            var f = true;
        case 'ArrowLeft':
            videoSeek(!!f);
            break;
        case 'ArrowUp': navigator.volumeManager.requestUp(); break;
        case 'ArrowDown': navigator.volumeManager.requestDown(); break;
        case 'SoftLeft': toggleVideoInfo();break;
        case 'SoftRight':actionsShow(true);break;
        case 'Backspace':
            if(getFullScreenElement()) {
                fsToggle(false);
            } else {
                backHistory();
            }
            break;
    }
  
}
