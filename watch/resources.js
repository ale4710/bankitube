(()=>{
    //scripts
    [
        'etc',
        'prompt',
        'playlist',
        'segment-skipping',
        'alt-rating-data',
		'video-speed-control',
        'quality-selector',
        'init',
        'actions',
        'actions-overflow',
        'captions',
        'captions-renderer',
        'control',
        'videoinfo',
        'comments',
        'comments-ui'
    ].forEach((fn)=>{
        addGlobalReference(0, fn);
    });

    //styles
    [
        'style',
        'fullscreen-style',
        'comments',
        'actions'
    ].forEach((fn)=>{
        addGlobalReference(1, fn);
    });
})();