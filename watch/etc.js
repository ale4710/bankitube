function videoLoaderShow(show,text) {
    if(show) {
        if(!text){text = i18n('loading')}
        document.getElementById('videoLoaderCont').style.display = null;
        document.getElementById('videoLoaderText').textContent = text;
    } else {
        document.getElementById('videoLoaderCont').style.display = 'none';
    }
}

var videoEvents = {
    play: function(){
        changeTimeBarVisibility(false);
        audioPlay();
    }, //PLAY AND PLAYING ARE DIFFERENT!!!
    canplay: function(){
        console.log('canplay');
        if(++output.mediaLoaded[0] === output.mediaLoaded[1] * 2) {beginPlaying();}
    },
    loadedmetadata: function(){
        console.log('loadedmetadata');
        if(++output.mediaLoaded[0] === output.mediaLoaded[1] * 2) {beginPlaying();}
    },
    pause: function(){
        changeTimeBarVisibility(true);
        updateStatusIcon('pause');

        audioAction('pause');

        updatenavbar();
    },
    playing: function(){
        videoLoaderShow(false);
        updateStatusIcon('play');
        audioPlay();
        updatenavbar();
    },
    seeking: function(){
        audioAction('pause');
        updateStatusIcon('wait');
    },
    seeked: function(){
        if(!output.video.paused){changeTimeBarVisibility(false);}
        audioPlay();
        updateStatusIcon('play');
    },
    stalled: function(){
        videoLoaderShow(true,i18n('video-buffering'));
        audioAction('pause');
        updateStatusIcon('wait');
    },
    waiting: function(){
        videoLoaderShow(true,i18n('video-waiting'));
        audioAction('pause');
        updateStatusIcon('wait');
    },
    error: function(e){
        console.error(e);videoLoaderShow(true,i18n('unknown-error'));
        updateStatusIcon('warning');
        videoAbleToPlay = false;
    },
    srcError: function(e){
        console.error(e);
        showErrorScreen(i18n('video-screen-error-failed-to-load'));
        alertMessage(i18n('video-toast-message-failed-to-load'),8000,3);
        updateStatusIcon('warning');
        videoAbleToPlay = false;
        playlistNextVideoDTE();
    },
	ratechange: function() {
		if(output.audio) {
			output.audio.playbackRate = output.video.playbackRate;
		}
	},
    ended: function(){
        if(!output.video.loop) {
            changeTimeBarVisibility(true);
            if(!playlistMode && !playlistReady) {
                videoLoaderShow(true,i18n('video-ended'));
                updateStatusIcon('stop');
            }
            audioAction('pause');
            updatenavbar();
            switch(getSettingValue('video-time-display')) {
                default: output.videoTime.text.textContent = '00:00'; break;
                case 1: output.videoTime.text.textContent = timeformat(output.video.duration); break;
                case 2: 
                    output.videoTime.text.textContent = timeformat(output.video.duration);
                    output.videoTime.textSub.textContent = '00:00';
                    break;
            }
            output.videoTime.bar.style.width = '100%';
            
            playlistNextVideo();
        }
    },
    timeupdate: function(){
        //???
        if(!output.video) {return}

        //variable init
        var vl = output.video.duration, videoLengthTH = 1, audioSyncTH = 0.5;

        //set just in case videoLength is borked
        if(
            videoLength - vl > videoLengthTH ||
            isNaN(vl)
        ) {
            vl = videoLength;
        }

        //segment skipping
        if(
            segmentSkipping.userCategories.length !== 0 &&
            segmentSkipping.toSkip.length !== 0
        ) {
            var timeSkip = checkSegmentToSkip(output.video.currentTime, true);

            switch(segmentSkipping.autoSkip + ((timeSkip !== false) << 1)) {
                case 0: //all false
                    eid('videoSkipOverlayCont').classList.add('hidden');
                    break;
                //case 1: //autoskip ON  timeskip FALSE
                case 2: //autoskip OFF timeskip TRUE
                eid('videoSkipOverlayCont').classList.remove('hidden');
                    if(timeSkip[0] + 5 < output.video.currentTime) {
                        eid('videoSkipOverlayCont').classList.add('minimized');
                    } else {
                        eid('videoSkipOverlayCont').classList.remove('minimized');
                    }
                    break;
                case 3: //autoskip ON  timeskip TRUE
                    console.log('sponsorblock: skipped to ' + timeSkip);
                    absVideoSeek(timeSkip[1]);
            }
        }

        //audio resync
        if(output.audio) {
            if(
                Math.abs(output.video.currentTime - output.audio.currentTime) > audioSyncTH
            ) {
                console.log(`audio resync: ${Math.abs(output.video.currentTime - output.audio.currentTime).toFixed(3)} (${output.video.currentTime.toFixed(3)} - ${output.audio.currentTime.toFixed(3)}). Attempt #${audioResyncAttempts}`);
                if(audioResyncAttempts++ > 10) {
                    audioResyncAttempts = 0;
                    audioResyncTimeout = setTimeout(()=>{output.video.play()},500);
                    output.video.pause();
                    output.audio.pause();
                }
                output.audio.currentTime = output.video.currentTime;
            } else {
                audioResyncAttempts = 0;
            }
        }

        //update the bar
        var ct = output.video.currentTime / vl;
        output.videoTime.bar.style.width = (ct * 100) + '%';
        switch(getSettingValue('video-time-display')) {
            default: //also 0
                output.videoTime.text.textContent = timeformat(vl - output.video.currentTime); break;
            case 1: 
                output.videoTime.text.textContent = timeformat(output.video.currentTime); break;
            case 2: 
                output.videoTime.text.textContent = timeformat(output.video.currentTime);
                output.videoTime.textSub.textContent = timeformat(vl - output.video.currentTime);
                break;
        }
    }
},
audioResyncAttempts = 0,
audioResyncTimeout = 0,
alraedyBeganPlaying = false,
videoAbleToPlay = false,
timeBarVisibliltyTO = 0,
tallVideo = true;

if('mozAudioChannelManager' in navigator) {
	navigator.mozAudioChannelManager.addEventListener('headphoneschange',function(){
		if(!navigator.mozAudioChannelManager.headphones) {
			if(!disableControls) {
				output.video.pause();
			}
		}
	}); //headhpone handeler,eg if unplug
}

function updateStatusIcon(ico) {
    document.getElementById('pause-indicator').src = '/img/' + ico + '.png';
}

function beginPlaying() {
    if(!alraedyBeganPlaying) {
        alraedyBeganPlaying = true;
        videoAbleToPlay = true;

        if(
            !segmentSkipping.timebarLabeled &&
            !segmentSkipping.getting
        ) {
            labelTimeBarWithSkipSegments();
        }

        if(
            continueTime !== 0 &&
            !askContinueTime
        ) {
            absVideoSeek(continueTime);
            continueTime = 0;
        }

        if(
            getSettingValue('video-autoplay') === 1 ||
            (getSettingValue('video-autoplay') === 2 && playlistMode) ||
            forceAutoPlay
        ) {
            forceAutoPlay = false;
            output.video.play();
            audioPlay();
            videoLoaderShow(false);
        } else {
            videoLoaderShow(true,i18n('video-ready-to-play'));
            updateStatusIcon('stop');
            updatenavbar();
        }

        showErrorScreen(false);

        if('videoWidth' in output.video) {
            tallVideo = output.video.videoWidth / output.video.videoHeight <= 1.1;
        } //else tallVideo = true (already set)

        output.video.dataset.tall = tallVideo;

        rotateIfFullscreen();
    }
}

function audioAction(act,set) {
    var ae = output.audio;
    if(ae) {
        if(set === undefined) {
            ae[act]();
        } else {
            ae[act] = set;
        }
    }
}
function audioPlay() {
    audioAction('play');
}

function videoSeek(forward) {
    changeTimeBarVisibility(true);
    absVideoSeek(output.video.currentTime + videoSeekAmount * ((-1) + (Number(forward) * 2)));
}
function absVideoSeek(time) {
    output.video.currentTime = time;
}

function postExtraAlert(cnt, cstyle) {
    var d = document.createElement('div');
    d.style = cstyle || '';
    d.innerHTML = cnt;
    eid('data-extraAlert').appendChild(d);
}

function toggleVideoLoop() {
    output.video.loop = !output.video.loop;

    if(output.video.loop) {
        eid('loop-indicator').classList.remove('hidden');
    } else {
        eid('loop-indicator').classList.add('hidden');
    }

    changeTimeBarVisibility(true);
    changeTimeBarVisibility(false);
}

function toggleVideoFit() {
    var vd = output.video, 
    fosd = eid('video-fill-osd'),
    fosdTxt = eid('video-fill-osd-text'),
    
    fits = [
        'contain',
        'cover',
        'fill'
    ],
    
    cfit = Math.max(fits.indexOf(vd.style.objectFit), 0) + 1;

    if(!(cfit in fits)) {
        cfit = 0;
    }

    vd.style.objectFit = fits[cfit];

    fosdTxt.textContent = i18n('video-' + 
        [
            'fit',
            'zoom',
            'fill'
        ][cfit]
    );
    fosd.classList.remove('hidden');
    animations.fadeOut(fosd);
}

function changeTimeBarVisibility(vis) {
    clearTimeout(timeBarVisibliltyTO);
    if(vis) {
        document.body.classList.add('timevisible');
    } else {
        timeBarVisibliltyTO = setTimeout(()=>{document.body.classList.remove('timevisible');},3000);
    }
}

function getFullScreenElement() {
    if('mozFullScreenElement' in document) {
        return document.mozFullScreenElement;
    } else {
        return document.fullscreenElement;
    }
}

function fsToggle(force) {
    if(force === undefined) {
        force = !getFullScreenElement();
    }
    switch((force << 1) + !!getFullScreenElement()) {
        case 1: //unfullscreen, is fullscreen
            document.mozCancelFullScreen();
            break;
        case 2: //fullscreen, is not fullscreen
            document.body.mozRequestFullScreen();
            break;
    }
}

function rotateScreen(hori) {
    if('orientation' in window.parent.screen) {
		if(hori && !tallVideo) { //don't rotate screen if video is "tall"
			return window.parent.screen.orientation.lock('landscape-primary');
		} else {
			return window.parent.screen.orientation.lock('portrait-primary'); //lock in portrait on load...
		}
    } else {
        return Promise.reject(Error('screen rotation is unsupported.'));
    }
}
rotateScreen(false);
document.addEventListener('fullscreenchange',()=>{
    console.log('fullscreen change');
    var fsel = document.getElementById('fullscreen-button');
    if(getFullScreenElement()) {
        fsel.src = '/img/buttons/exitfullscreen.png';
        rotateScreen(true);
    } else {
        fsel.src = '/img/buttons/fullscreen.png';
        rotateScreen(false);

    }
});
function rotateIfFullscreen() {
    if(getFullScreenElement()) {rotateScreen(true);}
}

document.addEventListener('visibilitychange',()=>{
    if(document.visibilityState === 'hidden'){
        rotateScreen(false);
        fsToggle(false);
    }
});

function addSourceToMediaElement(mel, url){
	let srcTag = document.createElement('source');
	srcTag.src = url;
	mel.appendChild(srcTag);
}

function outputRecommends(recs) {
    eid('actions-relatedvids').innerHTML = '';
    for(var i = 0; i < recs.length; i++) {
        var 
        main = document.createElement('div'),
        img = document.createElement('img'),
        time = document.createElement('div');

        main.appendChild(img);
        main.appendChild(time);

        if(i < 6) {
            img.src = thumbchoose(recs[i].videoThumbnails);
        } else {
            img.src = '/img/thumbnail-loading.png';
            img.dataset.src = thumbchoose(recs[i].videoThumbnails);
        }
        img.loading = 'lazy';


        time.textContent = timeformat(recs[i].lengthSeconds);

        main.dataset.title = recs[i].title;
        main.dataset.author = recs[i].author;
        main.dataset.id = recs[i].videoId;
        main.tabIndex = i;

        if(playlistMode) {
            if(i === playlistIndex) {
                main.classList.add('nowplaying');
            }
        }

        eid('actions-relatedvids').appendChild(main);
    }

    noRelateds = false;
    eid('actions-vertical-positioner').classList.remove('vertical-center');
    eid('actions-relatedvids-cont-wt').classList.remove('hidden');
    eid('actions-relatedvids-loading-placeholder').classList.add('hidden'); 
}

function showErrorScreen(text) {
    var veo = eid('videoErrorOverlay');
    if(text === false) {
        veo.classList.add('hidden');
    } else {
        veo.classList.remove('hidden');
        eid('videoErrorText').textContent = text;
    }
}

function exitToMain() {
    while(curpage > 0) {
       switch(curpage) {
            case 1: toggleVideoInfo(); break;
            case 2: 
            case 3: actionsShow(false); break;
            case 4: captionMenuShow(false); break;
            case 5: closeCommentsSubMenu(); break;
            case 6: hideVideoActionOverflowMenu(); break;
            case 7: qualityMenuDone(); break;
			case 8: speedControl.hide(); break;
            case 100: shareotReturn(); break;
            case 99: hideMainMenu();break;
            case 98: disableMinimize(); break;
			default:
				throw Error('Throwing to protect from infinite loop.');
       }
    }
}


function localupdatenavbar() {
    var nv = ['','',''];
    lunc: switch(curpage) {
        case 0: 
            nv[0] = i18n('info');
            nv[1] = videoAbleToPlay? (output.video.paused? createImg('/img/play.png') : createImg('/img/pause.png')) : '';
            nv[2] = i18n('actions');
            break;
        case 1:
            var uf = null;
            switch(infoTabList[infoTab].id) {
                case 'comments': uf = navigateCommentsUpdateNavbar; break;
                default: 
                    nv[0] = i18n('back');
                    break lunc;
            }
            nv = uf();
            break;
        case 2:
            nv[0] = i18n('menu');
            nv[1] = i18n('select');
            nv[2] = i18n('back');
            break;
        case 3:
            nv[0] = i18n('menu');
            nv[1] = listenMode? i18n('listen') : i18n('watch');
            nv[2] = i18n('back');
            break;
        case 4:
        case 5:
        case 6:
        case 7:
            nv[0] = i18n('back');
            nv[1] = i18n('select');
            break;
		case speedControl.pagen:
			nv = speedControl.updatenavbar();
			break;
    }
    outputNavbar(nv);
}