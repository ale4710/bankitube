var speedControl = (function(){
	var interface = {};
	
	var thisPage = 8;
	interface.pagen = 8;
	
	var nb = [i18n('back')];
	interface.updatenavbar = function() {return nb;};
	
	var pfHandler = new PreviousFocusHandler();
	
	var header = i18n('speed-control');
	var sliders = new optionsMenuMultiSliders.OptionsMenuMultiSliders(header);
	
	sliders.addOption(
		header,
		undefined,
		0.25,
		2,
		0.05,
		1
	);
	
	header = undefined;
	
	interface.reset = function(){
		sliders.resetAll();
	};
	
	interface.keyhandler = optionsMenuMultiSliders.genericKeyHandler(sliders,
		function(_, newValue) { //update fn, when the value updates
			if(output.video) {
				output.video.playbackRate = newValue;
			}
		},
		function(k){ //keyhandler (in addition to the given ones)
			switch(k.key) {
				case 'Backspace':
				case 'SoftLeft':
					hide();
					break;
			}
		}
	);
	
	interface.switchTo = (function(
		cb = emptyFn
	){
		pfHandler.clear();
		pfHandler.save(cb);

		curpage = thisPage;
		sliders.menuViewToggle(true, true);
	});

	function hide() {
		pfHandler.loadall();
		pfHandler.execcallback();
		pfHandler.clear();

		sliders.menuViewToggle(false);
	};
	interface.hide = hide;
	
	return interface;
})();