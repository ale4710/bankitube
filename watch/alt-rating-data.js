var altRatingDataRq = null;

function abortGetAltRatingData() {
    if(altRatingDataRq) {
        altRatingDataRq.abort();
        altRatingDataRq = null;
    }
}

function showAltRatingDataNote() {
    output.data.rating.altRatingDataNote.classList.remove('hidden');
}

function getAltRatingData() {
    abortGetAltRatingData();
    var card = itemCache( //card = cached alt rating data
        'altRatingData',
        videoId,
        'get'
    );

    if(card) {
        if(card.exists) {
            if(card.disabled) {
                printRatings(false);
            } else {
                printRatings(
                    card.likes,
                    card.dislikes,
                    card.viewCount
                );
            }

            showAltRatingDataNote();
        }
    } else {
        function icNoExist(){
            itemCache(
                'altRatingData',
                videoId,
                'update',
                {exists: false}
            );
        }
    
        altRatingDataRq = freedatagrab(
            getSettingValue('alt-rating-data-instance') + '/votes?videoId=' + videoId,
            (data/* , ed, rq */)=>{
                if(typeof(data) === 'object') {
                    if(
                        data.likes === 0 &&
                        data.dislikes === 0 &&
                        data.viewCount === 0
                    ) {
                        itemCache(
                            'altRatingData',
                            videoId,
                            'update',
                            {
                                disabled: true,
                                exists: true
                            }
                        );
                    } else {
                        printRatings(
                            data.likes,
                            data.dislikes,
                            data.viewCount
                        );
                        itemCache(
                            'altRatingData',
                            videoId,
                            'update',
                            {
                                likes: data.likes,
                                dislikes: data.dislikes,
                                viewCount: data.viewCount,
                                disabled: false,
                                exists: true
                            }
                        );
                    }

                    showAltRatingDataNote();
                } else {
                    icNoExist();
                }
            },
            icNoExist,
            false,
            true
        );
    }
}