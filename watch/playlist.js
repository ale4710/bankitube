var playlistPage = 1, 
playlistReady = false, 
playlistMode = false, 
userPlaylistMode = false, 
playlistItems = [],
playlistAvailable = [false,false]
;

function loadPlaylistInit() {
    eid('playlist-indicator').classList.remove('hidden');
    if(playlistId === sessionStorage.getItem('playlistCacheId')) {
        playlistItems = JSON.parse(sessionStorage.getItem('playlistCache'));
        loadPlaylistDone();
    } else {
        playlistReady = false;
        eid('actions-relatedvids-loading-placeholder').classList.remove('hidden');
        loadPlaylist();
    }
}

function loadPlaylist() {
    if(userPlaylistMode) {
        getUserPlaylistASR(
            playlistId,
            (plitems)=>{
                loadPlaylistRqDone({
                    title: userPlaylistMdataCache()[playlistId].name,
                    videos: plitems
                });
            }
        );
    } else {
        //videoId and playlistId should be defined
        requestItem(
            'playlists',
            playlistId,
            '?fields=title,videos&page=' + playlistPage,
            loadPlaylistRqDone,
            null,
            true,
            true
        );
    }
    
    console.log('Loading Playlist...!', playlistPage);
}

function loadPlaylistRqDone(rqResp) {
    console.log('loadPlaylistDone');
    playlistItems = playlistItems.concat(rqResp.videos);
    if(
        rqResp.videos.length === 100 && //does not return more than 100 per page. if there are 100 check to see if there are more
        0 in rqResp.videos && //0 in rqResp.videos is a redundant check.
        !userPlaylistMode //if userplaylist everything is given at once so no need to recheck every 100...
    ) {
        playlistPage++;
        loadPlaylist();
        return;
    }

    var curIndex = 0;
    while(curIndex < playlistItems.length) {
        if(curIndex in playlistItems) {
            var cpi = playlistItems[curIndex],
            vd = vidIsDed(cpi);
            
            if(vd) {
                playlistItems.splice(curIndex, 1);
            } else {
                playlistItems[curIndex].index = curIndex;
                curIndex++;
            }
        } else {
            break;
        }
    }

    sessionStorage.setItem('playlistCache',JSON.stringify(playlistItems));
    sessionStorage.setItem('playlistCacheId',playlistId);
    sessionStorage.setItem('playlistCacheName',rqResp.title);

    loadPlaylistDone();
}

function loadPlaylistDone(){

    playlistReady = true;
    playlistPage = undefined; //clean

    var count = document.createElement('span'), label = document.createElement('span');
    count.id = 'actions-relatedvids-text-count';

    console.log(playlistItems);

    for(var i = 0; i < playlistItems.length; i++) {
        if(playlistItems[i].videoId === videoId) {
            playlistIndex = i;
            break;
        }
    }

    count.textContent = playlistIndex + 1 + '/' + playlistItems.length;
    label.textContent = sessionStorage.getItem('playlistCacheName');

    eid('actions-relatedvids-text').textContent = ''; //delet
    eid('actions-relatedvids-text').appendChild(count);
    eid('actions-relatedvids-text').appendChild(label);

    outputRecommends(playlistItems);
    playlistItems = [];
}

function playlistNextVideo(force, fromUser) {
    if(playlistMode && playlistReady) {
        if(force === undefined) {
            force = 0;
        } else {
            force = (!force) * 2;
        }
        var rvs = eid('actions-relatedvids').children,
        npi = playlistIndex + (1 - force);
        if(playlistIndexExists(npi)) {
            var nv = rvs[npi];
            showPlaylistChange(npi + 1, rvs.length);
            loadAnotherVideo(nv.dataset.id, npi);
            //init();
        } else if(!fromUser) {
            if(playlistIndex === rvs.length - 1) {
                videoLoaderShow(true,i18n('video-playlist-ended'));
                playlistEndedNotification();
            }
        }
    }
}
function playlistNextVideoDTE() { //playlist next video due to error
    if(playlistMode) {
        alertMessage(
            i18n('video-toast-message-playlist-skip-due-to-error'),
            5000,
            2
        );
        playlistNextVideo();
    }
}

var showPlaylistChangeTO = 0;
function showPlaylistChange(n,t) {
    clearTimeout(showPlaylistChangeTO);
    showPlaylistChangeTO = setTimeout(()=>{
        eid('playlist-change-display').classList.add('hidden');
    },2000);
    eid('playlist-change-display-text').textContent = commaSeparateNumber(n) + '/' + commaSeparateNumber(t);
    eid('playlist-change-display').classList.remove('hidden');
}

function playlistIndexExists(index) {return index in eid('actions-relatedvids').children;}

function playlistEndedNotification() {
    document.removeEventListener('visibilitychange',playlistEndedNotification);
    if(document.visibilityState === 'hidden') {
        document.addEventListener('visibilitychange',playlistEndedNotification);
        if(getSettingValue('notify-playlist-end') === 1) {
            new Notification(
                i18n('video-playlist-ended-toast-message'),
                {icon: '/img/icon/112.png'}
            );
        }
    } else {
        alertMessage(i18n('video-playlist-ended-toast-message'),10000,0);
    }
}