var commentsLoaded = 0,
commentsFirstLoaded = false,
commentsExist = null,
commentsLoading = false,
commentsContinuation = null,
commentsDisplayCount = 0,
commentsRequester = null,
commentsLastFocused = null,
commentsImagesEnabled = !!getSettingValue('comments-user-images'),

commentsViewingReply = false,
commentsReplyCommentEl = null,
commentsReplyCurId = null,
commentsLocalCache = null,
commentsLocalReplyCache = {},
commentsCacheSpaceIds = ['comments', 'commentReply'],

commentsLoadMoreElId = 'data-comments-load-more',
commentsReturnToMainElId = 'data-comments-return-to-main',
commentsThrobberId = 'data-comments-loading';

document.addEventListener('DOMContentLoaded',()=>{
    eid('data-comments').addEventListener('scroll',()=>{
        updatenavbar();
    });

    screen.orientation.addEventListener('change',()=>{
        var expandedList = eid('data-comments').getElementsByClassName('expanded');
        while(expandedList.length !== 0) {
            expandedList[0].classList.remove('expanded');
        }
    });
});

function resetComments() {
    lightResetComments();
    commentsLocalCache = null;
    commentsLocalReplyCache = {};
    commentsLastFocused = null;
    commentsFirstLoaded = false;
    commentsReplyCommentEl = null;
	commentsExist = null;
    infoTabDisplay.tabs.byId['comments'].element.textContent = i18n('video-info-comments');
}
function lightResetComments() {
    if(commentsLoading) {commentsRequester.abort();}
    commentsLoading = false;
    commentsLoaded = 0;
    commentsDisplayCount = 0;
    commentsContinuation = null;
    commentsViewingReply = false;
    commentsReplyCurId = null;
}

function getMoreCommentsAllowed() {
    var cs = eid('data-comments');
    return cs.scrollHeight === cs.scrollTop + cs.clientHeight &&
    !!commentsContinuation &&
    !commentsLoading;
}

function getComments(commentId, replyContinuation, returnNumber) {
    if(
		commentsLoading ||
		(commentsExist === false)
	) {return}

    var commentGetId = videoId,
    ccache = commentsLocalCache;

    if(commentId && replyContinuation) {
        commentGetId = commentId;
        ccache = commentsLocalReplyCache[commentGetId];
    }

    if(commentsViewingReply === !commentId) {
        lightResetComments();
        if(
            ( //we were previously on the main comment page, and are now switching to a reply
                !!commentId << 1 +
                commentsViewingReply
            ) === 2
        ) {
            commentsLastFocused = returnNumber || 0;
            //we have not cleared the current comment section yet.
            commentsReplyCommentEl = eid('data-comments').children[commentsLastFocused];

            //note: if we are coming from the other way around (reply -> main page),
            //all of the below will reset itself by "lightResetComments()"
            commentsContinuation = replyContinuation;
            commentsReplyCurId = commentGetId;
        }
        commentsViewingReply = !!commentId;

        console.log('getComments: switching reply view mode. reply = ', commentsViewingReply);
    }

    var ltCacheId = commentsCacheSpaceIds[Number(!!commentId)],
    outputInfo = {
        fromCache: false,
        reply: !!commentId,
        id: commentGetId,
        ltCacheId: ltCacheId
    };

    commentsLoading = true;

    if(!ccache) {
        ccache = itemCache(
            ltCacheId,
            commentGetId,
            'get'
        );
    }

    if(
        ccache && 
        commentsLoaded === 0
    ) {
        if(outputInfo.reply) {
            commentsLocalReplyCache[commentGetId] = ccache;
        } else {
            commentsLocalCache = ccache;
        }
        outputInfo.fromCache = true;
        setTimeout(()=>{outputComments(ccache, outputInfo);});
    } else {
        var cnt = '';
        if(commentsContinuation) {
            cnt = '&continuation=' + commentsContinuation;
        }

        commentsRequester = requestItem(
            'comments',
            videoId,
            cnt,
            outputComments,
            commentsFailure,
            false,
            true,
            outputInfo
        );

        if(commentsLoaded > 0) {
            eid('data-comments').scrollTop = eid('data-comments').scrollHeight - eid('data-comments').clientHeight;
        }
    }

    var cn = document.createElement('center');
    cn.id = commentsThrobberId;
    cn.appendChild(makeThrobber());
    if(commentsLoaded === 0) {
        cn.classList.add('center');
        eid('data-comments').textContent = '';
        eid('data-comments').appendChild(cn);
        eid('data-comments').scrollTop = 0;
    } else {
        eid(commentsLoadMoreElId).innerHTML = '';
        eid(commentsLoadMoreElId).appendChild(cn);
        eid('data-comments').scrollTop = eid('data-comments').scrollHeight;
    }
    commentsLoaded++;
}

function outputCommentsMdataIcon(path) {
    var ico = createImg(path);
    ico.classList.add('icon');
    return ico;
}

function outputCommentsButton(label,id,tabindex) {
    var lm = document.createElement('div');
    lm.id = id;
    lm.textContent = label;
    lm.tabIndex = tabindex;
    lm.classList.add('text-center');
    return lm;
}

function outputCommentsMessage(message){
	eid('data-comments').innerHTML = '';
	var fm = document.createElement('div');
	fm.classList.add('center', 'text-center');
	fm.textContent = message;
	eid('data-comments').appendChild(fm);
}

function outputComments(rs, exdata) {
    console.log('outputComments recieved.',rs,exdata);

    var cs = rs.comments;

    if('continuation' in rs) {
        commentsContinuation = rs.continuation;
    } else {
        commentsContinuation = null;
    }
    
    if(commentsLoaded === 1) {
		commentsExist = true;
		
        eid('data-comments').innerHTML = '';
        
        if(cs.length === 0) {
            outputCommentsMessage(i18n('video-info-no-comments'));
            return;
        } else {
            if(
                'commentCount' in rs &&
                !commentsFirstLoaded
            ) {
                //for some reason it sometimes is not defined when pulling from cache.
                //unsure why this happens, so this is in an "if" for now.
                infoTabDisplay.tabs.byId['comments'].element.textContent += ` (${commaSeparateNumber(rs.commentCount)})`;
            }

            if(exdata.reply) {
                commentsDisplayCount = 2;
                eid('data-comments').appendChild(
                    outputCommentsButton(
                        i18n('return-to-main-comments'),
                        commentsReturnToMainElId,
                        0
                    )
                );
                commentsReplyCommentEl.tabIndex = 1;
                commentsReplyCommentEl.classList.add('data-comments-comment-top-reply');
                var replyBorder = document.createElement('div');
                replyBorder.classList.add('data-comments-comment-top-reply-border');
                replyBorder.textContent = i18n('replies');
                commentsReplyCommentEl.appendChild(replyBorder);
                eid('data-comments').appendChild(commentsReplyCommentEl);
                commentsReplyCommentEl = null;
            }
            infoTabDisplay.focusTab(infoTab);
        }
    } else if(commentsLoaded > 1) {
        eid(commentsLoadMoreElId).remove();
    }

    //create a comments test box
    var commentTestBox = document.createElement('div'),
    screenDims = [
        screen.width,
        screen.height
    ];
    commentTestBox.classList.add('data-comments-container');
    commentTestBox.style.opacity = 0;
    document.body.appendChild(commentTestBox);
    //get the values of the screen dimentions
    //determine which is vertical and horizontal
    if(checkIfScreenHori()) { //not portrait
        screenDims.unshift(screenDims.pop());
    }

    for(var i = 0; i < cs.length; i++) {
        var cur = cs[i],
        allContainer = document.createElement('div');

        //metadata
        allContainer.dataset.id = cur.commentId;
        allContainer.dataset.channelId = cur.authorId;

        var cmet = document.createElement('div');
        cmet.textContent = cur.content;
        cmet.classList.add('data-comments-comment');

        var cme = document.createElement('div');
        cme.classList.add('data-comments-comment-container');
        cme.appendChild(cmet);
 
        var cmem = document.createElement('div');
        if(commentsImagesEnabled) {
            var pic = createImg('/img/userimageloading.png');
            pic.dataset.src = imgChoose(cur.authorThumbnails,'width',100);
            pic.classList.add('data-comments-img');
            cmem.appendChild(pic);
        } else {
            cme.classList.add('data-comments-comment-container-noimg');
        }
        cmem.appendChild(cme);

        //metadata display
        var metadataItems = [];
        //inserted into array in display order from left to right

        //edited indicatior
        if(cur.isEdited) {
            metadataItems.push(outputCommentsMdataIcon('/img/pencil-tiny.png'));
        }

        //date posted
        var cdate = document.createElement('span');
        cdate.textContent = unixdateformat(cur.published);
        metadataItems.push(cdate);

        //reply count
        if('replies' in cur) {
            allContainer.dataset.replyContinuation = cur.replies.continuation;
            var crep = document.createElement('span');
            crep.appendChild(outputCommentsMdataIcon('/img/reply-tiny.png'));
            crep.innerHTML += commaSeparateNumber(cur.replies.replyCount);
            metadataItems.push(crep);
        }

        //like count
        var clikes = document.createElement('span');
        clikes.appendChild(outputCommentsMdataIcon('/img/rating/up-mc-and-mask.svg'));
        clikes.innerHTML += commaSeparateNumber(cur.likeCount);
        metadataItems.push(clikes);

        var cmd = document.createElement('div');
        cmd.classList.add('data-comments-metadata');
        metadataItems.forEach((el)=>{
            cmd.appendChild(el);
        });

        var nm = document.createElement('div');
        nm.classList.add('data-comments-name');
        if(cur.authorIsChannelOwner) {
            nm.classList.add('data-comments-name-channelowner');
        }
        nm.textContent = cur.author;

        allContainer.tabIndex = commentsDisplayCount;

        allContainer.appendChild(nm);
        allContainer.appendChild(cmem);
        allContainer.appendChild(cmd);
        //eid('data-comments').appendChild(allContainer); //dont append to main box yet. first test the height.

        if(!('expandData' in cur)) {
            //testing...
            commentTestBox.style.width = screenDims[0] + 'px'; //set portrait width
            commentTestBox.appendChild(allContainer); //append comment box
            var phi = allContainer.clientHeight; //portrait height initial
            commentTestBox.style.width = screenDims[1] + 'px';  //set landscape width
            var lhi = allContainer.clientHeight; //landscape height initial
            allContainer.classList.add('expandable', 'expanded');
            var lhf = allContainer.clientHeight; //landscape height final
            commentTestBox.style.width = screenDims[0] + 'px';  //set portrait width
            var phf = allContainer.clientHeight; //portrait height final

            //console.log(phi, phf, '//', lhi, lhf);

            if(
                phi === phf &&
                lhi === lhf
            ) {
                allContainer.classList.remove('expandable');
                cur.expandData = {};
            } else {
                cur.expandData = {
                    portrait: phi !== phf,
                    landscape: lhi !== lhf
                };
            }
            allContainer.classList.remove('expanded');
        }

        if('portrait' in cur.expandData) {
            allContainer.classList.add('expandable');
            var mi = document.createElement('div');
            mi.classList.add('data-comments-comment-expand-prompt');
            mi.textContent = i18n('more-ellipses');
            cme.appendChild(mi);

            if(cur.expandData.portrait) {allContainer.classList.add('expandable-portrait');}
            if(cur.expandData.landscape) {allContainer.classList.add('expandable-landscape');}
        }

        eid('data-comments').appendChild(allContainer);

        if(commentsLoaded > 1 && i === 0) {
            allContainer.focus();
            navigateCommentsLazyLoadHandler();
        }

        commentsDisplayCount++;
    }

    commentTestBox.remove();

    if(commentsContinuation) {
        eid('data-comments').appendChild(
            outputCommentsButton(
                i18n('load-more-ellipses'),
                commentsLoadMoreElId,
                commentsDisplayCount
            )
        );
    }

    commentsLoading = false;
    if(commentsLoaded === 1) {
        navigateCommentsFF();
    }

    //cache stuff
    var sendToCache = rs,
    ccache = commentsLocalCache;
    
    if(exdata.reply) {
        ccache = commentsLocalReplyCache[exdata.id];
    }
    
    if(!exdata.fromCache && ccache) {
        ccache.comments = ccache.comments.concat(cs);
        ccache.continuation = commentsContinuation;
        sendToCache = ccache;
    }
    
    if(exdata.reply) {
        commentsLocalReplyCache[exdata.id] = sendToCache;
    } else {
        commentsLocalCache = sendToCache;
    }
    itemCache(
        exdata.ltCacheId,
        exdata.id,
        'update',
        sendToCache
    ); 

    console.log(exdata.ltCacheId,exdata.id,'cache set to',sendToCache);

    commentsFirstLoaded = true;
    updatenavbar();
}

function commentsFailure(e) {
	if(e.status === 404) {
		outputCommentsMessage(i18n('video-info-no-comments'));
		commentsExist = false; //explicitly set this.
	} else {
		alertMessage(i18n('video-info-comments-failed-to-load'), 5000, 2);
		if(commentsLoaded === 1) {
			outputCommentsMessage(i18n('video-info-comments-failed-to-load'));
		} else {
			eid(commentsLoadMoreElId).textContent = i18n('load-more-ellipses');
		}
	}
    commentsLoading = false;
    commentsLoaded--;
}