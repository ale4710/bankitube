function relatedK(k) {
    switch(k.key) {
        case 'ArrowLeft':  k.preventDefault(); relatedVidScroll(-1); break;
        case 'ArrowRight': k.preventDefault(); relatedVidScroll(1); break;
        case 'ArrowUp':    navFocus();break;
        case 'SoftLeft': navMainMenu(0);fsToggle(false); break;
        case 'Enter':      loadAnotherVideo(actEl().dataset.id, actEl().tabIndex); fsToggle(false); /* actionsShow(false); */ break;
        case 'SoftRight':
        case 'Backspace':
            actionsShow(false); break;
    }
}
function relatedFocus() {
    if(!noRelateds) {
        eid('actions-relatedvids').scrollTo(0,0);
        eid('actions').classList.add('relatedsel');
        if(playlistMode) {
            relatedVidScroll(playlistIndex,true);
        } else {
            relatedVidScroll(0,true);
        }
        curpage = 3;

        relatedVidScrollText.play();
    }
}

var relatedVidScrollText = new ScrollingText(
    eid('actions-relatedvids-curvid-title'),
    null,
    5
);

function relatedVidScroll(dir,abs,nf) {
    var rvs = eid('actions-relatedvids'),//rvs = related videos
    rvsc = rvs.children; 

    if(rvsc.length === 0) {return}

    for(var i = 0; i < rvs.children.length; i++) {
        rvsc[i].classList.remove('active');
    }

    if(abs) {
        nextel = rvsc[dir];
    } else {
        //dir should be either -1 or 1. we are not going to check tho
        nextel = actEl().tabIndex + dir;
        if(!(nextel in rvsc)) {
            nextel += rvsc.length * (dir * -1);
        }
        nextel = rvsc[nextel];
    }
    if(!nf) {
        nextel.focus();
        nextel.classList.add('active');
    }
    rvs.scrollTo(
        nextel.offsetLeft + (nextel.offsetWidth / 2) - 
        (rvs.offsetWidth / 2)
    ,0);

    lazyLoadHandler(3,nextel,rvsc);

    eid('actions-relatedvids-curvid-title').textContent = nextel.dataset.title;
    eid('actions-relatedvids-curvid-author').textContent = nextel.dataset.author;
    updatePlaylistNumberDisplay(nextel.tabIndex + 1);
    relatedVidScrollText.reset();
}

function updatePlaylistNumberDisplay(num) {
    if(playlistMode && playlistReady) {
        var tl = eid('actions-relatedvids').children.length;
        if(!num) {
            num = playlistIndex + 1;
        }
        eid('actions-relatedvids-text-count').textContent = num + '/' + tl;
    }
}

function navActions(mv) {
    navigatelist(
        actEl().tabIndex,
        eid('actions-video').children,
        mv
    );
    navActionLabelUpdate();
}
function navFocus(customEl) {
    if(!customEl) {customEl = 0}
    eid('actions-relatedvids').scrollTo(0,0);
    eid('actions').classList.remove('relatedsel');
    eid('actions-video').children[customEl].focus();
    navActionLabelUpdate();
    relatedVidScroll(0,true,true);
    relatedVidScrollText.pause();
    updatePlaylistNumberDisplay();
    curpage = 2;
}
function navActionLabelUpdate() {
    var a = actEl().dataset.action, s,
    slb = (t)=>{eid('actions-video-label').textContent = t}, 
    ap = (ip)=>{return a + '-' + ip};
    switch(a) {
        default: s = a; break;
        case 'toggle-listen-mode': 
            s = ap(listenMode? 'watch' : 'listen'); break;
        case 'fullscreen-video': 
            s = ap(getFullScreenElement()? 'exit': 'fullscreen'); break;
        case 'view-channel':
            slb(channelName); return;
        case 'overflow-menu':
            slb(i18n('more-ellipses')); return;
    }
    slb(i18n('video-action-' + s));
}
function actionK(k) {
    switch(k.key) {
        case 'ArrowLeft':
            var l = -1;
        case 'ArrowRight': 
            navActions(l || 1);  
            break;
        case 'ArrowDown':  relatedFocus(); break;
        case 'SoftLeft': navMainMenu(0);fsToggle(false); break;
        case 'Enter':      actionsSelect(actEl().dataset.action); break;
        case 'SoftRight':
        case 'Backspace':
            actionsShow(false); break;
    }
}
function actionsShow(tr) {
    if(tr) {
        changeTimeBarVisibility(true);
        eid('actions').classList.remove('relatedsel','hidden');
        if(tallVideo) {fsToggle(false);}
        navFocus();
    } else {
        if(!output.video.ended && !output.video.paused) {changeTimeBarVisibility(false);}
        eid('actions').classList.add('hidden');
        curpage = 0;
        relatedVidScrollText.pause();
        relatedVidScroll(0,true,true);
        updatePlaylistNumberDisplay();
    }
}

function actionsRetToOverflow() {
	navFocus(5);
	updatenavbar();
}
function actionsSelect(act) {

    //returning true here will tell the caller to not change the page if it is going to do so.

    if(act !== 'fullscreen-video') {fsToggle(false);}
    switch(act) {
        case 'share-video': 
            shareot(
                linkUtil.formYtVideoLink(videoId),
                2
            );
        break;
        case 'fullscreen-video': fsToggle(); actionsShow(false); break;
        case 'captions-menu': captionMenuShow(true); break;
        case 'toggle-listen-mode': switchListenMode(); break;
        case 'view-channel': location = '/channel/index.html#' + channelId; break;
        case 'overflow-menu': showVideoActionOverflowMenu(); break;
        case 'seek-to': 
            //temporary thing for now~
            var ts = window.prompt('Put in the number of seconds you want to seek to.\n\nExample:\nSeek to 2:33 -> Type 153');
            if(ts === null) {break;}
            ts = parseInt(ts);
            if(
                !isNaN(ts) &&
                isFinite(ts) &&
                ts > -1 &&
                ts < output.video.duration
            ) {
                absVideoSeek(ts);
                changeTimeBarVisibility(true);
                changeTimeBarVisibility(false);
            } else {
                alertMessage('That was not a valid timestamp.', 3000, 0);
            }
            break;
        case 'loop-toggle': toggleVideoLoop(); break;
        case 'add-to-playlist': 
            addToPlaylistSDAE(
                videoId,
                actionsRetToOverflow,
                null
            );
            return true;
        case 'change-video-fit': toggleVideoFit(); break;
        case 'toggle-segment-skipping': toggleSegmentSkipping(); break;
        case 'quality-menu':
            qualityMenuShow(); 
            return true;
		case 'speed-control-menu':
			speedControl.switchTo(actionsRetToOverflow);
			return true;
    }
}